#include <avr/io.h> 
#include <avr/wdt.h>
#include <avr/pgmspace.h> 
#include <Arduino.h>
#include "ht1632c_font.h"
#include "ht1632c.h"
#include "digitalWriteFast.h"

#undef abs
#include <stdlib.h>

#define digitalWriteFast2 digitalWriteFast

/******************************************************************************
 * Constructors
 ******************************************************************************/

/**
 * Constructor for one display panel
 * 
 */
ht1632c::ht1632c(byte nDisplays)
 : _displays(nDisplays)
{
	set_display_geometry(HT1632_GEOMETRY_X, HT1632_GEOMETRY_Y);
	setup(HT1632_DATA_PIN, HT1632_WRCLK_PIN);
	_proportional = false;
}

/******************************************************************************
 * public API
 ******************************************************************************/

/**
 * Plot a dot
 *
 * @param char x
 * @param char y
 * @param char val
 */

byte ht1632c::get_width()
{
  return _geometry_x*_displays;
}

byte ht1632c::get_height()
{
    return _geometry_y;
}

void ht1632c::plot (char x, char y, char val) {
  
  char addr, bitval;
  if (x<0 || x>=get_width() || y < 0 || y >= _geometry_y) return;
  byte d = x / _geometry_x;
  x = x - _geometry_x*d;

  /*
   * The 4 bits in a single memory word go DOWN, with the LSB (first transmitted) bit being on top.  However, writebits()
   * sends the MSB first, so we have to do a sort of bit-reversal somewhere.  Here, this is done by shifting the single bit in
   * the opposite direction from what you might expect.
   */

  bitval = 8>>(y&3);  // compute which bit will need set

  addr = (x<<2) + (y>>2);  // compute which memory word this is in 

  byte old_data = _shadowram[(d * 96)  + addr];

  if (val) {  // Modify the shadow memory
    _shadowram[(d * 96)  + addr] |= bitval;
  } 
  else {
    _shadowram[(d * 96) + addr] &= ~bitval;
  }
  
  // Now copy the new memory value to the display
  if (_direct_write) {

    //if(old_data != _shadowram[(d * 96) + addr])
  	  senddata(d, addr, _shadowram[(d * 96) + addr]);
  }
}

void ht1632c::clear() {
	char i;
	for(byte d=0; d<_displays; d++)
	{
		chipselect(d);  // Select chip

		writebits(HT1632_ID_WR, 1<<2);  // send ID: WRITE to RAM
		writebits(0, 1<<6); // Send address
		for (i = 0; i < 96/2; i++) // Clear entire display
			writebits(0, 1<<7); // send 8 bits of data
    chipfree(d);  // Select chip
		for (i=0; i < 96; i++)
			_shadowram[96*d + i] = 0;
	}
}

void ht1632c::set_proportional( bool proport) {
	_proportional = proport;
}

void ht1632c::set_font(byte width, byte height, bool proport/*=false*/) {
	_font_width = width;
	_font_height = height;
	_proportional = proport;
}

byte ht1632c::get_font_width() {
  return _font_width;
}

byte ht1632c::get_font_height() {
  return _font_height;
}


byte ht1632c::put_char(int x, int y, unsigned char c) {
  byte dots, temp_char_width=_font_width, temp_char_start=0, bRet;
  
  if( _font_width==3)
  {
    if( strchr( ":;.!", c) )                { temp_char_width -= 2; temp_char_start = 1; }
	if( c=='_')                             { temp_char_width -= 2; temp_char_start = 1; c = ' ';} // _ are short space 
  }
  else if( _font_width==4)
  {
    if( strchr( ":;.!", c) )                { temp_char_width -= 3; temp_char_start = 1; }
    if( strchr( " ,1ITY[]{}|^fijklrt", c) ) { temp_char_width -= 1; temp_char_start = 0; }
    if( c==133)                             { temp_char_width -= 1; temp_char_start = 1; }  // deg
	if( c=='_')                             { temp_char_width -= 3; temp_char_start = 1; c = ' ';} // _ are short space 
  }
  else if( _font_width==5 && _proportional)
  {
    if( strchr( ":;.!", c) )                { temp_char_width -= 4; temp_char_start = 2; }
    if( strchr( " ,1IY[]{}|^ijl", c) )      { temp_char_width -= 2; temp_char_start = 1; }
    if( c==133 )                            { temp_char_width -= 2; temp_char_start = 1; }  // deg
    if( strchr( "tr", c) )                  { temp_char_width -= 2; temp_char_start = 0; }
    if( strchr( "fg", c) )                  { temp_char_width -= 1; temp_char_start = 1; }
    if( strchr( "abcdehknopsuvyz", c) )     { temp_char_width -= 1; temp_char_start = 0; }
    if( c==130 || c==131 || c==132 )        { temp_char_width -= 1; temp_char_start = 0; }
	if( c=='_')                             { temp_char_width -= 4; temp_char_start = 2; c = ' ';} // _ are short space 
  }

  bRet = temp_char_width+1;
  if( _font_width==5 && _proportional && strchr( "T", c) ) bRet--;

  
  if( x + temp_char_width < 0 || x >= get_width() || y + get_font_height() < 0 || y >= _geometry_y) return bRet;

  c -= 32; // offset
  //Serial.print(_displays); Serial.print("Disp ");
  //Serial.print(temp_char_width); Serial.print("x"); Serial.println(_font_height); 
  if (_font_height <= 8) {
	for (char col=temp_char_start; col< temp_char_width+temp_char_start; col++) {  // 0, 1, 2, 3
		switch (_font_width) {
//#ifndef HT1632_ONLY_5X7
		case 3:
			dots = pgm_read_byte_near(&ht1632c_font_3x5[c][col]);
		break;
		case 4:
			dots = pgm_read_byte_near(&ht1632c_font_4x7[c][col]); 
		break;
//#endif
		case 5:
			dots = pgm_read_byte_near(&ht1632c_font_5x7[c][col]);
		break;
		case 7:
			dots = pgm_read_byte_near(&ht1632c_font_7x7[c][col]);
		break;
		}
        byte rowRead;
		for (char row=0; row < _font_height; row++) {
            rowRead = _font_height==7 ? row : 2+row;
      		if (bitRead(dots, 1+rowRead))  // 7-row (7, 6, 5, 4, 3, 2, 1)
        		plot(x+col-temp_char_start, y+row, 1);
      		else 
        		plot(x+col-temp_char_start, y+row, 0);
    	}
  	}
  } else {
//#ifndef HT1632_ONLY_5X7
	for (byte row=0; row < _font_height; row++)
	{
		dots = pgm_read_byte_near(&ht1632c_font_5x12[c][row]);
		for (byte col=temp_char_start; col<temp_char_width; col++)
		{
			if (bitRead(dots, 1+col))  // 7-col
				plot(x+col-temp_char_start, y+row, 1);
			else 
				plot(x+col-temp_char_start, y+row, 0);
		}
	}
//#endif
  }
  // clear pixel row follow sign:
  line(x+temp_char_width, y, x+temp_char_width, y+_font_height, 0);
  // clear area over small signs:
  if( _font_height<7 )
	  rect(x, y+_font_height, x+temp_char_width, y+6, 0);
  
  return bRet;
}

void ht1632c::putstring(int x, int y, unsigned char* c) {
  byte i =0;
  while(c[i]) 
  {
    if( c[i]==195 )
    {
      i++;
      switch( c[i])
      {      
        case 132: x += put_char(x, y, (unsigned char)127); break;  // Ä
        case 150: x += put_char(x, y, (unsigned char)128); break;  // Ö
        case 156: x += put_char(x, y, (unsigned char)129); break;  // Ü
        case 164: x += put_char(x, y, (unsigned char)130); break;  // ä
        case 182: x += put_char(x, y, (unsigned char)131); break;  // ö
        case 188: x += put_char(x, y, (unsigned char)132); break;  // ü
      }
    }
    else
      x += put_char(x, y, (unsigned char)c[i]);

    i++;
  }
}

void ht1632c::flashing_cursor(byte xpos, byte ypos, byte cursor_width, byte cursor_height, byte repeats) {
  for (byte r = 0; r <= repeats; r++) {    
    for (byte x = 0; x <= cursor_width; x++) {
      for (byte y = 0; y <= cursor_height; y++) {
        plot(x+xpos, y+ypos, 1);
      }
    }
    
    if (repeats > 0) {
      delay(400);
    } else {
      delay(70);
    }
        
    for (byte x = 0; x <= cursor_width; x++) {
      for (byte y = 0; y <= cursor_height; y++) {
        plot(x+xpos, y+ypos, 0);
      }
    }   
    //if cursor set to repeat, wait a while
    if (repeats > 0) {
     delay(400); 
    }
  }
}

void ht1632c::onoff(bool bOn) {
    for( short d=0; d<_displays; d++ ) {
      if( bOn ) sendcmd(d, HT1632_CMD_LEDON);  //LEDs ON
      else      sendcmd(d, HT1632_CMD_LEDOFF); //send ED duty cycle gen off to displays
	}
}

void ht1632c::fade_down() {
  char intensity;
  for (intensity=_intensity; intensity >= 0; intensity--) {
    for( short d=0; d<_displays; d++ )
      sendcmd(d, HT1632_CMD_PWM + intensity); //send intensity commands using CS0 for display d
    delay(_fadedelay);
  }
  //clear the display and set it to full brightness again so we're ready to plot new stuff
  clear();
  for( short d=0; d<_displays; d++ )
    sendcmd(d, HT1632_CMD_PWM + _intensity); //send intensity commands using CS0 for display 0
}

void ht1632c::fade_up() {
  char intensity;
  for ( intensity=0; intensity <= _intensity; intensity++) {
   for( short d=0; d<_displays; d++ )
      sendcmd(d, HT1632_CMD_PWM + intensity); //send intensity commands using CS0 for display 0
    delay(_fadedelay);
  }
}

/******************************************************************************
 * private methods
 ******************************************************************************/
byte ht1632c::CS_PIN(byte display) {
  switch(display)
  {
    case 0: return HT1632_CS1_PIN; break;  // Select chip
    case 1: return HT1632_CS2_PIN; break;
    case 2: return HT1632_CS3_PIN; break;
  }
}

void ht1632c::chipselect (byte chipno) {
  switch(chipno)
  {
    case 0: digitalWriteFast2(HT1632_CS1_PIN, 0); break;
    case 1: digitalWriteFast2(HT1632_CS2_PIN, 0); break;
    case 2: digitalWriteFast2(HT1632_CS3_PIN, 0); break;
  }
}

void ht1632c::chipfree (byte chipno) {
  switch(chipno)
  {
    case 0: digitalWriteFast2(HT1632_CS1_PIN, 1); break;
    case 1: digitalWriteFast2(HT1632_CS2_PIN, 1); break;
    case 2: digitalWriteFast2(HT1632_CS3_PIN, 1); break;
  }
}

void ht1632c::writebits (byte bits, byte firstbit) {
	while (firstbit) {
		digitalWriteFast(HT1632_WRCLK_PIN, LOW);
		if (bits & firstbit) {
		  digitalWriteFast(HT1632_DATA_PIN, HIGH);
		} 
		else {
		  digitalWriteFast(HT1632_DATA_PIN, LOW);
		}
		digitalWriteFast(HT1632_WRCLK_PIN, HIGH);
		firstbit >>= 1;
	}
}

void ht1632c::sendcmd (byte d, byte command) {
  chipselect(d);  // Select chip
	writebits(HT1632_ID_CMD, 1<<2);  // send 3 bits of id: COMMMAND
	writebits(command, 1<<7);        // send the actual command
	writebits(0, 1);         	  // one extra dont-care bit in commands.
  chipfree(d);  // done
}

void ht1632c::senddata (byte d, byte address, byte data) {
  chipselect(d);  // Select chip
	writebits(HT1632_ID_WR, 1<<2); // Send ID: WRITE to RAM
	writebits(address, 1<<6);      // Send address
	writebits(data, 1<<3);         // Send 4 bits of data
  chipfree(d);  // done
}


void ht1632c::setup(byte data, byte wrclk) {
	
  _shadowram = (byte *) malloc(96*_displays);


	for (byte d=0; d<_displays; d++) {
		pinMode(CS_PIN(d), OUTPUT);

    switch(d) {
      case 0: digitalWriteFast(HT1632_CS1_PIN,  HIGH); break;  // Unselect (active low)
      case 1: digitalWriteFast(HT1632_CS2_PIN, HIGH); break;
      case 2: digitalWriteFast(HT1632_CS3_PIN, HIGH); break;
    }
		
		pinMode(HT1632_WRCLK_PIN, OUTPUT);
		pinMode(HT1632_DATA_PIN, OUTPUT);

		sendcmd(d, HT1632_CMD_SYSON);    // System on 
		sendcmd(d, HT1632_CMD_LEDON);    // LEDs on 
		sendcmd(d, HT1632_CMD_COMS01);   // NMOS Output 24 row x 24 Com mode
		// master/slave:
		//if(d==0) sendcmd(d, HT1632_CMD_MSTMD);
		//else sendcmd(d, HT1632_CMD_SLVMD);
	    //sendcmd(d, HT1632_CMD_MSTMD);

		for (byte i=0; i<128; i++)
	  		senddata(d, i, 0);  // clear the display!
	}
	
	direct_write(true);
}

byte ht1632c::get_shadowram(byte x, byte y) {
  byte addr, bitval;

	// TODO: rewrite;
  //select display depending on plot values passed in
  byte d = x / _geometry_x;
  x = x - _geometry_x*d; 

  bitval = 8>>(y&3);  // compute which bit will need set
  addr = (x<<2) + (y>>2);       // compute which memory word this is in 
  return (0 != (_shadowram[(d * 96) + addr] & bitval));
}

void ht1632c::snapshot_shadowram() {
	for (byte i=0; i< sizeof _shadowram; i++) {
		_shadowram[i] = (_shadowram[i] & 0x0F) | _shadowram[i] << 4;  // Use the upper bits
	}
}

byte ht1632c::get_snapshotram(byte x, byte y) {
  byte addr, bitval;

  byte d = x / _geometry_x;
  x = x - _geometry_x*d;

  bitval = 128>>(y&3);  // user upper bits!
  addr = (x<<2) + (y>>2);   // compute which memory word this is in 
  if (_shadowram[(d * 96) + addr] & bitval)
    return 1;
  return 0;
}

void ht1632c::set_display_geometry(byte x, byte y) {
	_geometry_x = x;
	_geometry_y = y;
}

/* graphic primitives based on Bresenham's algorithms */

void ht1632c::line(int x0, int y0, int x1, int y1, byte color)
{
  int dx =  abs(x1-x0), sx = x0<x1 ? 1 : -1;
  int dy = -abs(y1-y0), sy = y0<y1 ? 1 : -1; 
  int err = dx+dy, e2; /* error value e_xy */

  for(;;) {
    plot(x0, y0, color);
    if (x0 == x1 && y0 == y1) break;
    e2 = 2*err;
    if (e2 >= dy) { err += dy; x0 += sx; } /* e_xy+e_x > 0 */
    if (e2 <= dx) { err += dx; y0 += sy; } /* e_xy+e_y < 0 */
  }
}

// cursor x pixel to right
void ht1632c::space(int dx)
{
	int i;
	for( i=0; i<dx; i++ )
	{
	  line(_x, _y, _x, _y+_font_height+1,0);
	  _x++;
	}
}


void ht1632c::rect(int x0, int y0, int x1, int y1, byte color)
{
  line(x0, y0, x0, y1, color); /* left line   */
  line(x1, y0, x1, y1, color); /* right line  */
  line(x0, y0, x1, y0, color); /* top line    */
  line(x0, y1, x1, y1, color); /* bottom line */
}

void ht1632c::circle(int xm, int ym, int r, byte color)
{
  int x = -r, y = 0, err = 2-2*r; /* II. Quadrant */ 
  do {
    plot(xm - x, ym + y, color); /*   I. Quadrant */
    plot(xm - y, ym - x, color); /*  II. Quadrant */
    plot(xm + x, ym - y, color); /* III. Quadrant */
    plot(xm + y, ym + x, color); /*  IV. Quadrant */
    r = err;
    if (r >  x) err += ++x * 2 + 1; /* e_xy+e_x > 0 */
    if (r <= y) err += ++y * 2 + 1; /* e_xy+e_y < 0 */
  } while (x < 0);
}

void ht1632c::ellipse(int x0, int y0, int x1, int y1, byte color)
{
  int a = abs(x1 - x0), b = abs(y1 - y0), b1 = b & 1; /* values of diameter */
  long dx = 4 * (1 - a) * b * b, dy = 4 * (b1 + 1) * a * a; /* error increment */
  long err = dx + dy + b1 * a * a, e2; /* error of 1.step */

  if (x0 > x1) { x0 = x1; x1 += a; } /* if called with swapped points */
  if (y0 > y1) y0 = y1; /* .. exchange them */
  y0 += (b + 1) / 2; /* starting pixel */
  y1 = y0 - b1;
  a *= 8 * a; 
  b1 = 8 * b * b;

  do {
    plot(x1, y0, color); /*   I. Quadrant */
    plot(x0, y0, color); /*  II. Quadrant */
    plot(x0, y1, color); /* III. Quadrant */
    plot(x1, y1, color); /*  IV. Quadrant */
    e2 = 2 * err;
    if (e2 >= dx) { x0++; x1--; err += dx += b1; } /* x step */
    if (e2 <= dy) { y0++; y1--; err += dy += a; }  /* y step */ 
  } while (x0 <= x1);

  while (y0 - y1 < b) {  /* too early stop of flat ellipses a=1 */
    plot(x0 - 1, ++y0, color); /* -> complete tip of ellipse */
    plot(x0 - 1, --y1, color); 
  }
}

void ht1632c::bezier(int x0, int y0, int x1, int y1, int x2, int y2, byte color)
{
  int sx = x0 < x2 ? 1 : -1, sy = y0<y2 ? 1 : -1; /* step direction */
  int cur = sx * sy * ((x0 - x1) * (y2 - y1) - (x2 - x1) * (y0 - y1)); /* curvature */
  int x = x0 - 2 * x1 + x2, y = y0 - 2 * y1 + y2, xy = 2 * x * y * sx * sy;
                                /* compute error increments of P0 */
  long dx = (1 - 2 * abs(x0 - x1)) * y * y + abs(y0 - y1) * xy - 2 * cur * abs(y0 - y2);
  long dy = (1 - 2 * abs(y0 - y1)) * x * x + abs(x0 - x1) * xy + 2 * cur * abs(x0 - x2);
                                /* compute error increments of P2 */
  long ex = (1 - 2 * abs(x2 - x1)) * y * y + abs(y2 - y1) * xy + 2 * cur * abs(y0 - y2);
  long ey = (1 - 2 * abs(y2 - y1)) * x * x + abs(x2 - x1) * xy - 2 * cur * abs(x0 - x2);

  if (cur == 0) { line(x0, y0, x2, y2, color); return; } /* straight line */
     
  x *= 2 * x; y *= 2 * y;
  if (cur < 0) {                             /* negated curvature */
    x = -x; dx = -dx; ex = -ex; xy = -xy;
    y = -y; dy = -dy; ey = -ey;
  }
  /* algorithm fails for almost straight line, check error values */
  if (dx >= -y || dy <= -x || ex <= -y || ey >= -x) {        
    line(x0, y0, x1, y1, color);                /* simple approximation */
    line(x1, y1, x2, y2, color);
    return;
  }
  dx -= xy; ex = dx+dy; dy -= xy;              /* error of 1.step */

  for(;;) {                                         /* plot curve */
    plot(x0, y0, color);
    ey = 2 * ex - dy;                /* save value for test of y step */
    if (2 * ex >= dx) {                                   /* x step */
      if (x0 == x2) break;
      x0 += sx; dy -= xy; ex += dx += y; 
    }
    if (ey <= 0) {                                      /* y step */
      if (y0 == y2) break;
      y0 += sy; dx -= xy; ex += dy += x; 
    }
  }
}

/* returns the pixel value (RED, GREEN, ORANGE or 0/BLACK) of x, y coordinates */

byte ht1632c::getpixel (byte x, byte y)
{
	return get_shadowram(x,y);
}

/* boundary flood fill with the seed in x, y coordinates */

void ht1632c::fill_r(byte x, byte y, byte color)
{
  if(!getpixel(x, y))
  {
    plot(x, y, color);
    fill_r(++x, y ,color);
    x = x - 1 ;
    fill_r(x, y-1, color);
    fill_r(x, y+1, color);
  }
}

void ht1632c::fill_l(byte x, byte y, byte color)
{
  if(!getpixel(x, y))
  {
    plot(x, y, color);
    fill_l(--x, y, color);
    x = x + 1 ;
    fill_l(x, y-1, color);
    fill_l(x, y+1, color);
  }
}


void ht1632c::fill(byte x, byte y, byte color)
{
  fill_r(x, y, color);
  fill_l(x-1, y, color);
}


void ht1632c::scrolltext(int y, const char *text, int delaytime, int times, byte dir)
{
  int text_len =  strlen(text);

  int total_length = (_font_width + 1) * text_len;
  set_proportional(true);
	  
  while (times) {
    int x = 0;
    for ((dir) ? x = - total_length : x = get_width(); (dir) ? x < get_width() : x > - total_length; (dir) ? x++ : x--){
      putstring(x, y, (unsigned char *)text);
      //line(x-1, y, x-1, y+_font_height, 0);
      //line(x+total_length, y, x+total_length, y+_font_height, 0);
      delay(delaytime);
    }
    //line(x-1, y, x-1, y+_font_height, 0);
    //line(x+total_length, y, x+total_length, y+_font_height, 0);
    times--;
  }
  set_proportional(false);
}

// scroll text to end position, dir: 0=right, 1=left, 2=up, 3=down
void ht1632c::scrolltext_to(int x_end, int y, const char *text, int delaytime, int times, uint8_t dir) {
  int text_len =  strlen(text);

  uint8_t char_height = get_font_height();

  int total_length = 0;

  // Only needed because characters have variable widths
  for(int i = 0; i < text_len; i++){
    total_length += (_font_width + 1) * text_len;
  }

  while (times) {
    int x = 0;
	// horizontal?
	if( dir<=1 ){
      set_proportional(true);
      for ((dir) ? x = - total_length : x = get_width(); x != x_end + 1; (dir) ? x++ : x--){
        putstring(x, y, (unsigned char *)text);
        //line(x-1, y, x-1, y+char_height, 0);
        //line(x+total_length, y, x+total_length, y+char_height, 0);
        delay(delaytime);
      }
      set_proportional(false);
	}
	else{  // changing x <-> y
      for ((dir==2) ? x = - char_height : x = get_height(); x != x_end + 1; (dir==2) ? x++ : x--){
        putstring(y, x, (unsigned char *)text);
        //line(x-1, y, x-1, y+char_height, 0);
        //line(x+total_length, y, x+total_length, y+char_height, 0);
        delay(delaytime);
      }
	}
    //line(x-1, y, x-1, y+char_height, 0);
    //line(x+total_length, y, x+total_length, y+char_height, 0);
    times--;
  }
}



void ht1632c::direct_write(bool direct) {
	_direct_write = direct;
}

void ht1632c::render() {
  byte addr, bitval;

  for (byte x=0; x < _geometry_x; x++) {
	for (byte y=0; y < _geometry_y; y++) {
    	byte d = x / _geometry_x;
    	x = x - _geometry_x*d; 

  	  bitval = 8>>(y&3);  // compute which bit will need set
  	  addr = (x<<2) + (y>>2);       // compute which memory word this is in 
		  senddata(d, addr, _shadowram[(d * 96) + addr]);
    }
  }
}


void ht1632c::brightness(char intensity) {
	if (intensity < 0 || intensity >= 15) return;	
  for (byte d=0; d<_displays; d++) 
    sendcmd(d, HT1632_CMD_PWM + intensity); //send intensity commands using CS for display 
  _intensity = intensity;
}


// compatible functions like lcd-display 16x2 (2019-12-23):
void ht1632c::setCursor( byte col, byte line) {
  _x = col * (_font_width+1);
  _y = (1-line) * (_font_height+2);
}


void ht1632c::print(char *c) {
  byte i =0;
  
  //Serial.println(c);
  while(c[i] && _x<get_width()) {
    if( c[i]==-61 )
    {
      switch( c[i+1])
      {
        case -124: c[i+1] = 127; i++; break;
        case -106: c[i+1] = 128; i++; break;
        case -100: c[i+1] = 129; i++; break;
        case  -92: c[i+1] = 130; i++; break;
        case  -74: c[i+1] = 131; i++; break;
        case  -68: c[i+1] = 132; i++; break;
      }
    }
    _x += put_char(_x,_y, (unsigned char)c[i]);
    //_lcd_cur_raw++; 
    i++;
  }
}


void ht1632c::print(const __FlashStringHelper *c) {
  char buffer[30];
  memset(buffer, '\0', sizeof(buffer));
  strlcpy_P(buffer, (const char PROGMEM *)c, sizeof(buffer));
  print(buffer);
}

void ht1632c::println(char *txt) {
  print( txt );
  if( _y>0 )
  { 
    //_lcd_cur_line++; 
	//_y = (1-_lcd_cur_line)*(_font_height+2);
	_y -= _font_height+2;
	//_lcd_cur_raw = 0;
	_x = 0;
  }
}

void ht1632c::printByte(byte c) {
  _x += put_char(_x,_y, (unsigned char)c);
}
