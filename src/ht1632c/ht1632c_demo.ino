#include "ht1632c.h"
//#include <Adafruit_HT1632.h>
#include <digitalWriteFast.h>

// sure electronics 24x16 panel connected to pins
// DATA - pin 10
// WRCLK - pin 11
// CS1 - pin 4
ht1632c panel(3);
/*
#define HT_DATA 7
#define HT_WR   6
#define HT_CS   4
#define HT_CS2  3
#define clear       clearScreen
#define brightness  setBrightness
Adafruit_HT1632LEDMatrix panel = Adafruit_HT1632LEDMatrix(HT_DATA, HT_WR, HT_CS, HT_CS2);
*/
#define BRIGHTNESS 15  // 0..15
#define FONT   5,7    // 5,12  5,7  4,7  3,5

void setup()
{
   panel.set_font(FONT);
   panel.brightness(BRIGHTNESS); 
   Serial.begin(9600);
}

void loop() {
  panel.brightness(BRIGHTNESS); 
  demoFlashingCursor();
  demoAlphabethGross(); 
  demoAlphabethKlein(); 
  //demoScrollingText(); 
  //demoLines();
  demoRects();
  //demoCircles();
  demoFill();
}

void demoFlashingCursor() {
   panel.clear();
 //panel.flashing_cursor(0,1,5,6,1);   // x, y, width, height, repeats
   byte x = 0;
// char* text = "DEMO 3";
   char* text = " 1234567890";
   for (byte i=0; i<strlen(text); i++) {
      panel.flashing_cursor(i*(panel.get_font_width()+1),1,panel.get_font_width(),panel.get_font_height()-1,0);
      delay(5);
      panel.put_char(i*(panel.get_font_width()+1),1,text[i]);
      delay(200);
   }
   delay(2000);
   panel.fade_down();
}


void demoAlphabethKlein() {
   panel.brightness(BRIGHTNESS); 
   panel.clear();
   panel.set_font(FONT);
   char* text1 = "abcdefghijkl";
   char* text2 = "mnopqrstuvwx";
   for (byte i=0; i<strlen(text1); i++) {
      panel.put_char(i*(panel.get_font_width()+1),panel.get_font_height()+2,text1[i]);
      panel.put_char(i*(panel.get_font_width()+1),1,text2[i]);
   }
   
   delay(5000);
   panel.clear();
}


void demoAlphabethGross() {
   panel.brightness(BRIGHTNESS); 
   panel.clear();
   panel.set_font(FONT);
   char* text1 = "ABCDEFGHIJKL";
   char* text2 = "MNOPQRSTUVWX";
   for (byte i=0; i<strlen(text1); i++) {
      panel.put_char(i*(panel.get_font_width()+1),panel.get_font_height()+2,text1[i]);
      panel.put_char(i*(panel.get_font_width()+1),1,text2[i]);
   }
   
   delay(5000);
   panel.clear();
}


void demoScrollingText() {
  panel.clear();
  panel.fade_up();
  // y, Text, delay, times, dir
//panel.scrolltext(1, "WELCOME TO THE SURE ELECTRONICS DISPLAY DEMO", 30, 1, 0);
//panel.scrolltext(1, "Siderial GPS Clock, Goldan - 20.12.2019 19:13:45", 35, 1, 0);
  panel.scrolltext(4, "Frohe Weihnachten 24.12.2019 18:08:15", 10, 1, 0);
  delay(1000);
}

void demoLines() {
  for (byte i=0; i<25; i++) {
    byte x1 = random(0,panel.get_width()-1);
    byte y1 = random(0,15);
    byte x2 = random(x1-15,x1+15);
    byte y2 = random(0,15);
    panel.line(x1,y1,x2,y2,1);
    delay(70);
    //panel.fade_down();
    panel.clear();
  }
}

void demoRects() {
  static byte xm=0, ym=0, r=5;
  static byte xdir=1, ydir=1;

  for (byte j=0; j<240; j++) 
  {
    xm += xdir;
    ym += ydir;
    if( ym+r>14 ) ydir = -ydir;
    if( ym==0 ) ydir = 1;
    if( xm+r>panel.get_width()-2 ) xdir = -xdir;
    if( xm==0 ) xdir = 1;
    panel.rect(xm,ym,xm+r,ym+r,1);
    delay(25);
    panel.rect(xm,ym,xm+r,ym+r,0); 
    //delay(200);
    //panel.fade_down();
  }
  panel.rect(xm,ym,xm+r,ym+r,1);
  panel.clear();
}

void demoCircles() {
  for (byte j=0; j<20; j++) {
  byte xm = random(0,panel.get_width()-1);
  byte ym = random(0,15);
  byte r = random(1,12);
  if( j>15 ) { xm = panel.get_width()/2; ym = 8; r = 6; }
   for (byte i = 0; i<r; i++) {
     panel.circle(xm,ym,i,1);
      delay(50);
     panel.circle(xm,ym,i,0); 
   }
   panel.circle(xm,ym,r,1);
   //delay(200);
   //panel.fade_down();
   panel.clear();
  }
}

void demoFill() {
  byte xm=panel.get_width()/2, ym=8, r=0;

  for (byte j=0; j<panel.get_width()/2+1; j++)
  {
    panel.rect(xm-r,ym-r,xm+r,ym+r,1);
    r++;
    delay(70);
  }
  delay(300);
  panel.fade_down();
}
