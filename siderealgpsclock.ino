#include <Adafruit_BME280.h>

// For needed libraries see keyword "Library:"

// includes the needed library codes:
#include <TimeLib.h>
#include <sundata.h>

// build for Arduino model:
#define ARDU_MEGA        // for Arduino MEGA, other case UNO
// other special settings for software:
#undef VS                // specials for Volkssternwarte Hannover (3 panels)
#undef BLINK             // blinking LED for seconds
#undef DEBUG             // Debug outputs on Serial?
#undef BLINK_DP          // blinking double points at time
#define SEPARATOR '.'    // Separator in times for LED Matrix wie (VS :) ' ', ':', '.'
#define AUTO_RESET       // Reset of Arduino dayly at 04:00:00

// Sidereal Clock, GPS base
// Output of MEZ, MOZ, WOZ, STZ, JD, moon phase, temperature, humidity, preasure, textes
//
// Archive: https://gitlab.com/Goldan/siderialgpsclock.git
// Author:  Jürgen Goldan
// Mail:    juergen.goldan@gmail.com
// Web:     http://www.goldan.org/juergen/software

// which sensor should be used?
// important: if using LCD display, don't use BME280 weather sensor (not solved yet)
//            LED-Matrix only on Arduino Mega useful, not on Uno
#ifdef ARDU_MEGA 
  #define  LED_MATRIX      // if defined -> use LED-Matrix 16x24, else use LCD display
  #define  WEATHER_BME     // weather sensor BME280
  #undef   WEATHER_DMT     // weather sensor DMT11/22
#else
  #undef   LED_MATRIX      // if defined -> use LED-Matrix 16x24, else use LCD display
  #undef   WEATHER_BME     // weather sensor BME280
  #define  WEATHER_DMT     // weather sensor DMT11/22
#endif

// Welcome message
char Welcome1[]=" Sidereal GPS Clock              "; //the startup print on the LCD/LED
#ifdef LED_MATRIX
  char Welcome2[]=" - Jürgen Goldan, 15.04.2023     ";
#else  
  char Welcome2[]=" - Goldan, 15.04.2023            ";
#endif

// Defines of main functions:
void Output_Time(int x, int y,  int hh, int mm, int ss, short DoIt, char cSepa=SEPARATOR);
void Output_Blink(int sec);

void Output_Clock(int x, int y, int r, int hh, int mm);
void Output_Pos(int x, int y);
void Output_Date(int x, int y);
void Output_WeekDay(int x, int y);
void Output_UTC(int x, int y);
void Output_JD(int x, int y, int dd, int mm, int yy);
void Output_Moz(int x, int y);
void Output_Woz(int x, int y);
void Output_ZGL(int x, int y);
void Output_Stz(int x, int y);
void Output_Sun(int x, int y);
void Output_SunRiseSet(int x, int y, int secs);
void Output_MoonPercent(int x, int y);
void Output_MoonPhaseTxt(int x, int y);
void Output_Sensor(int x, int y, float val, float mean, float fOffset, short nWhat);
void Output_TempMinMax(int x, int y, int secs);
bool Output_Text( char *line1, char *line2, int Wait );
void Output_HumMinMax(int x, int y, int secs);

bool SetDisplayOn();      // switch display on
double b2pi( double x );  // 0..2*pi
double b24( double x );   // 0..23.99999
int imod(int z1, int z2); // modulo for integer
void dm( double deg, int *ih, double *dm, int *iv);
int doy(int y, int m, int d, int *wt);  // day of year

//**** Pins: ***************************************************************
// UNO:
//   A0  LED DAT
//   A1  LED #WR
//   A2  LED #RD (not used)
//   A3  LED #CS Chip select1
//   A4  LCD-display, BME sensor
//   A5  LCD-display, BME sensor
//   A6  LED #CS Chip select2
//   A7  LED #CS Chip select3
//   A4  DHT
//   D4  GPS RX
//   D5  GPS TX (not needed)
//   D6  LED red
//   D8  BUTTON_1 OFF/Brightness
//   D9  BUTTON_2 UT
//   D10 BUTTON_3 MOZ
//   D11 BUTTON_4 WOZ
//   D12 BUTTON_5 STZ
//   D13 BUTTON_6 Change others
// MEGA:
//   A0  LED DAT
//   A1  LED #WR
//   A2  LED #RD (not used)
//   A3  LED #CS Chip select1
//   A4  LED #CS Chip select2
//   A5  LED #CS Chip select3
//   TX1 GPS RX
//   RX1 GPS TX (not needed)
//   D53 LED red
//   D51 BUTTON_1 OFF/Brightness
//   D49 BUTTON_2 UT
//   D47 BUTTON_3 MOZ
//   D45 BUTTON_4 WOZ
//   D43 BUTTON_5 STZ
//   D41 BUTTON_6 Change times
//   D39 BUTTON_7 Change sum/moon
//   D37 BUTTON_8 Change wether
//*****************************************************************************

//**** Switching the output/display to:
#define FCT_BUTTON_2  4   // UTC
#define FCT_BUTTON_3  6   // MOZ
#define FCT_BUTTON_4  7   // WOZ
#define FCT_BUTTON_5  9   // STZ

//                       0   1   2   3   4   5   6   7   8   9  10   11  12  13  14  15  16  17  18  19
//                     POS Dat Day Clk UTC  JD MOZ WOZ ZGL STZ Sun RiSet Mo  Ph   T MiMa  H MiMa  P Txt
#ifndef DEBUG
  short nMapper[] =  {   0,  7,  0, 10,  0,  0,  0,  9,  0, 10,  7,  8,   7,  7,  8,  8,  6,  6,  7, 12, -1 }; // period showing (0 is off, -1 is last)
#else
  short nMapper[] =  {   0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,   3,  3,  0,  6,  6,  6,  6, -1 };   // period showing (0 is off, -1 is last)
#endif
#ifdef ARDU_MEGA  
  #ifdef VS
    short nMenuOthers[]  = {  1,  2,  3,  8,  5, -1 }; // displays for switching with button others   // displays for switching with button others (MATRIX 3)
    short nMenuSunMoon[] = { 10, 12, 13,  0, -1 };     // displays for switching with button SunMoon
    short nMenuWeather[] = { 14, 15, 16, 17, 18, -1 }; // displays for switching with button Weather
  #else
    short nMenuOthers[]  = { 14, 15, 16, 17, 18, 0, 1, 2, 3, 5, 4, 6, 7, 8, 9, 10, 11, 12, 13,  -1 }; // displays for switching with button others (MATRIX 2)
  #endif
#else
  short nMenuOthers[]    = {  14, 15, 16, 0, 1, 2, 5, 4, 6, 7, 8, 9, 10, 12, 13, -1 };                // displays for switching with button others (LCD)
#endif
short nCount = 0;             // counter for displaying each output, until nMapper[x] reached
short nDisplayMode = 0;       // 0=Pos, 1=Dat, 2=Day, 3=Clk, 4=UTC, 5=JD, 6=MOZ, 7=WOZ, 8=ZGL, 9=STZ, 10=Sun, 11=RiSet, 12=Mo, 13=Ph, 14=Temp, 15=MiMa, 16=Hum, 17=Press, 18=Text
short nDisplayOthers = 0;     // state of button other ...
#ifdef ARDU_MEGA
  short nDisplaySunMoon = 0;  // state of button SunMoon ...
  short nDisplayWeather = 0;  // state of button Weather ...
#endif
#ifdef VS
 long SecDisplayOff = 7200l;  // time in sec to switch display off (-1=never), LEDs off/LCD backlight off
  int SecAutoOn = 120;        // auto-mode switch ON after secs since last key pressed
#else
 long SecDisplayOff = 64800l; // time in sec to switch display off (-1=never), LEDs off/LCD backlight off
  int SecAutoOn = 300;        // auto-mode switch ON after secs since last key pressed
#endif
bool  bAutomatic = true;      // change display automatic to next view
bool  bModeStatic = false;    // no change of display?
bool  bDispOn = true;         // state of display on/off
int   SecGPSsync = 151;       // time in sec to sync time Arduino with GPS (600 =10min)
long  secDisplayOn=0l;        // secs since display on
long  secLastButton=0l;       // secs since last button pressed
long  secLastSync=0l;         // secs since last GPS time synced
bool  bHas2Clear = false;     // by next switch has to clear the display
bool  DoScrollingFirst = true;// scrolling from buttom as start with date


#ifdef LED_MATRIX
  //**** Display LED 16x24 Pixel (up to 3 panals) ***********************
  // Website: https://www.amazon.de/gp/product/B07Z7T9L5G/ref=ppx_yo_dt_b_asin_image_o04_s00?ie=UTF8&psc=1
  //   or
  // Website: https://https://de.aliexpress.com/item/32712599985.html?spm=a2g0o.productlist.0.0.fe224555XCvpcp
  // Library: ht632c in this project
  // Sensor            Arduino
  // --------------------------------------------------------------------
  // VCC               5V/3.3V
  // GND               Gnd
  // DAT               A0
  // #WR               A1
  // #RD               A2 (not used)
  // #CS Chip select1  A3 for panel 1 (see ht1632c.h)
  // for next panals:
  // #CS Chip select2  A4 for panel 2
  // #CS Chip select3  A5 for panel 3
  // --------------------------------------------------------------------
  // each panel is connected 1:1 to next panel (without CS!)
  // --------------------------------------------------------------------
 
  #include "src/ht1632c/ht1632c.h"
  // important: to set all pins for all panals -> changes in ht1632c.h, not in this file
  short   Display_x  = 12;         // all size display x
  short   Display_y  =  2;         // all size display y
  #ifdef VS
    short   Displays   =  3;       // all displays for VS   
  #else
    short   Displays   =  2;       // number of displays    
  #endif
  const short maxCharsLine = Displays*4; // max chars in display
  ht1632c display(Displays);       // number of displays (2, 3)
  const short maxBrightness = 15;
  #ifdef VS
    short   brightness = 3;        // 0, 3, 6, 9, 12, 15
  #else
    short   brightness = 0;        // 0, 3, 6, 9, 12, 15
  #endif

  #define CHAR_UMLAUT_A  127
  #define CHAR_UMLAUT_O  128
  #define CHAR_UMLAUT_U  129
  #define CHAR_UMLAUT_a  130
  #define CHAR_UMLAUT_o  131
  #define CHAR_UMLAUT_u  132
  #define CHAR_DEG       133       // ascii code ht1632c for '°'
  #define CHAR_UP        134
  #define CHAR_DOWN      135
  #define CHAR_SUN        32       // all charset 7x7 ...
  #define CHAR_STAR       33
  #define CHAR_MOON       38
  #define CHAR_MOON100    34
  #define CHAR_MOON83M    35
  #define CHAR_MOON67M    36
  #define CHAR_MOON50M    37
  #define CHAR_MOON33M    38
  #define CHAR_MOON17M    39
  #define CHAR_MOON0      40
  #define CHAR_MOON17P    41
  #define CHAR_MOON33P    42
  #define CHAR_MOON50P    43
  #define CHAR_MOON67P    44
  #define CHAR_MOON83P    45
  
#else
  //**** Display LCD 2x16 ***********************************************
  // name:I2C LCD1602
  // Website: https://www.sunfounder.com/i2clcd.html
  // Email:support@sunfounder.com
  // Library: LiquidCrystal_I2C
  // Sensor                  Arduino
  // --------------------------------------------------------------------
  // Pin 1   GND             GND
  // Pin 2   VCC             5V
  // Pin 3   SDA             ANALOG_IN A4
  // Pin 4   SDL             ANALOG_IN A5
  // --------------------------------------------------------------------

  #include <LiquidCrystal_I2C.h>
  #define set_font(x,y,b)  setDelay(0,0)   // dummy function to compile set_font
  short Displays   = 1;     // number of displays    
  short Display_x = 16;     // size display x
  short Display_y =  2;     // size display y
  // initialize the library with the numbers of the interface pins
  // set the LCD address to 0x27 for a 16 chars and 2 line display
  LiquidCrystal_I2C display(0x27,Display_x,Display_y); 
  const short maxCharsLine = 16;
  const short maxBrightness = 1;
  short brightness = 1;     // 0/1
  short    tim = 300;       // the value of delay time in running text

  #include "U8glib.h"
  #include <SPI.h>

  #define CHAR_DEG   0xdf
  #define CHAR_UP      0
  #define CHAR_DOWN    1
  #define CHAR_SUN     2
  #define CHAR_MOON    3
  #define CHAR_MOON100 4
  #define CHAR_MOON83M 4
  #define CHAR_MOON67M 5
  #define CHAR_MOON50M 5
  #define CHAR_MOON33M 5
  #define CHAR_MOON17M 6
  #define CHAR_MOON0   6
  #define CHAR_MOON17P 6
  #define CHAR_MOON33P 7
  #define CHAR_MOON50P 7
  #define CHAR_MOON67P 7
  #define CHAR_MOON83P 4
#endif


#ifdef WEATHER_BME   // with BME sensor, else DMT11/22
  //**** Weather sensor BME280 ******************************************
  // Quelle:  https://www.amazon.de/gp/product/B07D8T4HP6/ref=ppx_yo_dt_b_asin_title_o01_s00?ie=UTF8&th=1
  // Library: Adafruit_BME280 library
  // --------------------------------------------------------------------
  // Pin 1   VIN             5V
  // Pin 2   GND             GND
  // Pin 3   SCL             Mega 21, Uno A5
  // Pin 4   SDA             Mega 20, Uno A4
  // IC2 Protocol, device code 0x76
  // --------------------------------------------------------------------

  #include <Adafruit_BME280.h>

  #define SEALEVELPRESSURE_HPA (1013.25)
  Adafruit_BME280 dht_bme;
  #ifdef VS
    float fTempOffset = -8.0; // Offset for temperature sensor bei 22°
  #else
    float fTempOffset = -5.6; // Offset for temperature sensor, war -4.4 da nun mit Deckel
  #endif
  float fHumOffset  = 17.1;   // Offset for humidity sensor
  float fAirOffset = 0.0;     // Offset for air pressure
  float press;
#endif

#ifdef WEATHER_DMT
  //**** Weather sensor DHT11/22 ****************************************
  // Quelle: https://www.amazon.de/AZDelivery-AM2302-Temperatursensor-Luftfeuchtigkeitssensor-Arduino/dp/B06XF4TNT9/ref=sr_1_4?__mk_de_DE
  // Library: DHT sensor library
  // Sensor                  Arduino
  // --------------------------------------------------------------------
  // Pin 1   3.3-5V (left)   3.3V
  // Pin 2   Data            D3   mit R4,7K an +3.3V
  // Pin 3   Gnd
  // Pin 4   Gnd             GND
  // --------------------------------------------------------------------

  #include "DHT.h"
  #define DHTPIN 3              // Data Pin at analog port Pin A4 Arduino
  #define DHTTYPE DHT22         // DHT11, DHT21, DHT22
  DHT dht_bme(DHTPIN, DHTTYPE); // Weather Sensor
  float fTempOffset = -2.4;     // Offset for temperature sensor
  float fHumOffset  = -5.5;     // Offset for humidity sensor
#endif

float temp;                     // Temperature °C
float temp_min;                 // min. temperature per day
float temp_max;                 // max. temperature per day
float hum;                      // humidity %
float hum_min;                  // min. humidity per day
float hum_max;                  // max. humidity per day
float tt_mean = -999.0, hh_mean = -999.0, pp_mean = -999.0; // for trend
float fAnteil = 0.04;           // part of new measurement to mean (0.04)
float fSchwelle = 0.07;         // show arrow if value over level (0.04)
long  LastTempSec = 0, LastHumSec = 3, LastPressSec = 20;
short nCntMinMax = 0;           // switching min/max and rise/set display


//**** GPS ************************************************************
// Library: TinyGPS++  (http://arduiniana.org/libraries/tinygpsplus/)
//          import TinyGPSPlus-master.zip
//          SoftwareSerial
// Sensor                  Arduino
// ----------------------- Uno: ---------------------------------------
// Pin 1   3.3V (left)     3.3V
// Pin 2   RX   (green)    DIGITAL D4
// Pin 3   TX   (yellow)   DIGITAL D5 (not needed)
// Pin 4   Gnd             GND
// ------------------------ Mega: -------------------------------------
// Pin 1   3.3V (left)     5V
// Pin 2   RX   (green)    TX1
// Pin 3   TX   (yellow)   RX1 (not needed)
// Pin 4   Gnd             GND
// --------------------------------------------------------------------

#ifndef ARDU_MEGA
  #include <SoftwareSerial.h>   // only UNO needed
#endif
#include <TinyGPS++.h>
#if defined(ARDUINO) && ARDUINO >= 100
  #ifndef LED_MATRIX
    #define printByte(args)  write(args);
  #endif
#else
  #define printByte(args)  print(args,BYTE);
#endif
float gpsLat = 0.0, gpsLon = 0.0; // create variable for latitude and longitude object 
double dlnut, eps;
#ifdef ARDU_MEGA
  #define gpsSerial Serial1       // used for MEGA
#else
  SoftwareSerial gpsSerial(5,4);  //rx,tx  --> GPS TX (at Pin5), RX (at Pin4)
#endif
TinyGPSPlus gps;                  // create gps object
bool  bGPSrunning = true;

// setting of all pins for red LED and all buttons:
#ifdef ARDU_MEGA
  //**** Define LED for blinking ****************************************
  #define LED_RED    53 // LED Pin
  //**** Define buttons *************************************************
  #define BUTTON_1   51       // next, switch to LOW (Gnd)
  #define BUTTON_2   49       // UTC, switch to LOW (Gnd)
  #define BUTTON_3   47       // MOZ, switch to LOW (Gnd)
  #define BUTTON_4   45       // WOZ, switch to LOW (Gnd)
  #define BUTTON_5   43       // STZ, switch to LOW (Gnd)
  #define BUTTON_6   41       // others, switch to LOW (Gnd)
  #define BUTTON_7   39       // others, switch to LOW (Gnd)
  #define BUTTON_8   37       // others, switch to LOW (Gnd)
#else
  //**** Define LED for blinking ****************************************
  #define LED_RED    6
  //**** Define buttons *************************************************
  #define BUTTON_1   8        // next, switch to LOW (Gnd)
  #define BUTTON_2   9        // UTC, switch to LOW (Gnd)
  #define BUTTON_3  10        // MOZ, switch to LOW (Gnd)
  #define BUTTON_4  11        // WOZ, switch to LOW (Gnd)
  #define BUTTON_5  12        // STZ, switch to LOW (Gnd)
  #define BUTTON_6  13        // others, switch to LOW (Gnd)
#endif

/**********************************************************/
#ifdef VS
  //                           111111111122222222223333333333444444444455555555556
  //                 0123456789012345678901234567890123456789012345678901234567890
  char TextRun1[] = "        Wir freuen uns über jede Spende. ";                       // the string to print on display (LCD on line 1)
  char TextRun2[] = "Eine Spendenkasse befindet sich links neben der Tür. Danke !  ";  // the string to print on display (LCD on line 2)
#else  // nothing
  char TextRun1[] = "";
  char TextRun2[] = "";
#endif

// needed variables:
char sBuf[25];
char sfloat[12];
double rho;                 // = PI / 180
double pi2;                 // = PI * 2
unsigned long curMilli =0; 
bool   IsSynced = false;    // received GPS?
int    AnzSync = 0;         // Counter synched
int    TimeZone = 1;        // timezone: 0=UT, 1=MEZ
bool   WithDayTime = true;  // with summertime?
bool   NowDayTime = false;  // now it is summertime?
int    LastSecTime = -1;    // last sec for last time
double SynMonth = 29.530589;// length of synodic month (days)
int    Doy = -1;            // Day of year
int    BeginSummerTime = -1;// start date summertime (doy)
int    EndSummerTime = -1;  // end summertime (doy)
bool   ShowTime = true;     // mode show time in first line
int    LastDay = -1;        // to check new day
char   cLocation[20];       // name of location for MOZ output
int    DayOfWeek = -1;      // day of the week (0=Mo, 1=Di, ..)
int    NumberDaysMonth[] = {31,28,31,30,31,30,31,31,30,31,30,31};
const char   *DayShort[7] = { "Mo", "Di", "Mi", "Do", "Fr", "Sa", "So" };
const char   *DayLong[7]  = { "10Montag",     // 16 + 13 first are number of spaces before to center text
                              "5 Dienstag",   // 25 + 13
                              "5 Mittwoch",   // 25 + 13
                              "1 Donnerstag", // 37 + 13
                              "9 Freitag",    // 19 + 13
                              "7 Samstag",    // 22 + 13
                              "7 Sonntag" };  // 21 + 13

#ifndef LED_MATRIX
//**** user defined characters for LCD display:
uint8_t char_down[8] = // arrow down
{ 0b00000,
  0b00100,  //     *
  0b00100,  //     *
  0b00100,  //     *
  0b11111,  // * * * * *
  0b01110,  //   * * *
  0b00100}; //     *
  
uint8_t char_up[8] =   // arrow up
{ 0b00100,  //     *
  0b01110,  //   * * *
  0b11111,  // * * * * *
  0b00100,  //     *
  0b00100,  //     *
  0b00100,  //     *
  0b00000}; 
  
uint8_t char_sun[8]  =   // Sun
{ 0b00000,
  0b01110,  //   * * *
  0b10001,  // *       *
  0b10101,  // *   *   *
  0b10001,  // *       *
  0b01110,  //   * * *
  0b00000};
  
uint8_t char_moon[8] =  // Moon
{ 0b00000,
  0b01110,  //   * * *
  0b10100,  // *   *
  0b10100,  // *   *
  0b10100,  // *   *
  0b01110,  //   * * *
  0b00000};
  
uint8_t char_full[8] = // Moon 100%
{ 0b00000, 
  0b01110,  //   * * *
  0b11111,  // * * * * *
  0b11111,  // * * * * *
  0b11111,  // * * * * *
  0b01110,  //   * * *
  0b00000};    
  
uint8_t char_moonm50[8] = // Moon 50%-
{ 0b00000,
  0b01110,  //   * * *
  0b11001,  // * *     *
  0b11001,  // * *     *
  0b11001,  // * *     *
  0b01110,  //   * * *
  0b00000};
uint8_t char_new[8] = // Moon 0%
{ 0b00000,
  0b01110,  //   * * *
  0b10001,  // *       *
  0b10001,  // *       *
  0b10001,  // *       *
  0b01110,  //   * * *
  0b00000};
 
uint8_t char_moonp50[8] = // Moon 50%+
{ 0b00000,
  0b01110,  //   * * *
  0b10011,  // *     * *
  0b10011,  // *     * *
  0b10011,  // *     * *
  0b01110,  //   * * *
  0b00000};
#endif


/*********************************************************/
void setup() 
{
#ifdef BLINK
  pinMode(LED_RED, OUTPUT);        // red LED
#endif
  pinMode(BUTTON_1, INPUT_PULLUP); // Button 1, click to LOW
  pinMode(BUTTON_2, INPUT_PULLUP); // Button 2, click to LOW
  pinMode(BUTTON_3, INPUT_PULLUP); // Button 3, click to LOW
  pinMode(BUTTON_4, INPUT_PULLUP); // Button 4, click to LOW
  pinMode(BUTTON_5, INPUT_PULLUP); // Button 5, click to LOW
  pinMode(BUTTON_6, INPUT_PULLUP); // Button 6, click to LOW
  #ifdef VS
    pinMode(BUTTON_7, INPUT_PULLUP); // Button 7, click to LOW
    pinMode(BUTTON_8, INPUT_PULLUP); // Button 8, click to LOW
    nMapper[3] = nMapper[11] = nMapper[16] = nMapper[17] = 0;   // no Clock, rise/set, no humidity for VS
  #else
    #ifndef DEBUG
      nMapper[2] = 7; // with date
    #endif
    nMapper[19] = 0;  // no running text
  #endif
#ifndef ARDU_MEGA
  nMapper[2] = nMapper[3] = nMapper[11] = 0;  // no Clock, SunRiseSet
#endif

#ifdef WEATHER_BME
  dht_bme.begin(0x76);
#endif
#ifdef WEATHER_DMT
  dht_bme.begin();
  nMapper[18] = 0; // no pressure
#endif
  temp_min = 100.0;
  temp_max = -100.0;
  hum_min = 100.0;
  hum_max = -100.0;
  
#ifndef LED_MATRIX
  display.init();            //initialize the LCD display
  display.backlight();       //open the backlight
  display.createChar(0, char_up);
  display.createChar(1, char_down);
  display.createChar(2, char_sun);
  display.createChar(3, char_moon);
  display.createChar(4, char_full);
  display.createChar(5, char_moonm50);
  display.createChar(6, char_new);
  display.createChar(7, char_moonp50);
#endif
 
  digitalWrite(3,LOW); 
  digitalWrite(2,LOW); 
  rho = PI/180.0;
  pi2 = PI*2.0;
 
  setTime(20,00,00, 02,01,2021);  // HH,MM,SS, Day, Month, Year (UT)
  
#ifdef LED_MATRIX
  display.clear();
  display.set_font(5,7);
  display.brightness(brightness);

  /**** demo moon phases:
  int i;
  display.set_font(5,7,true);
  for( i=0; i<40; i++)
  {
    display._x = 0;
    display.print(" ");
    display.set_font(7,7);
    display.printByte(i%12+34);
    display.set_font(5,7,true);
    display.print(" ");
    delay(200);
  } */

  /* show clock for test:
  Output_Clock(display.get_width()/2, display.get_height()/2, 7, 18, 41);
  delay(20000); */

  /* display.print("AÄÖÜäöü ");
  delay(15000); */
  /*display.printByte('A');
  display.printByte(CHAR_UMLAUT_A);
  display.printByte(CHAR_UMLAUT_O);
  display.printByte(CHAR_UMLAUT_U);
  display.printByte(CHAR_UMLAUT_a);
  display.printByte(CHAR_UMLAUT_o);
  display.printByte(CHAR_UMLAUT_u);
  delay(15000); */

  #ifndef DEBUG
    //**** show running start text
    Welcome1[19] = Welcome2[30] = 0;
    char buf[60]; strcpy( buf, Welcome1 ); strcat( buf, Welcome2 );
    display.set_font(5,7,true);
    display.scrolltext(5, buf, 20, 1, 0);
    display.setCursor(0,0);
    display.set_font(5,7);
  #endif
  
#else
  //**** running on LCD:
  display.clear(); //Clears the LCD screen and positions the cursor in the upper-left  corner.
  display.setCursor(15,0); // set the cursor to column 15, line 0
  for (int positionCounter = 0; positionCounter < 32 && Welcome1[positionCounter]; positionCounter++)
  {
    display.scrollDisplayLeft(); //Scrolls the contents of the display one space to the left.
    display.print(Welcome1[positionCounter]); // Print a message to the LCD.
    //display.setCursor(15,1); display.print(array2[positionCounter]); // Print a message to the LCD.
    delay(tim); //wait for 250 microseconds
  }
  display.clear(); //Clears the LCD screen and positions the cursor in the upper-left  corner.
  display.setCursor(15,1); // set the cursor to column 15, line 1
  for (int positionCounter = 2; positionCounter < 32 && Welcome2[positionCounter]; positionCounter++)
  {
    display.scrollDisplayLeft(); //Scrolls the contents of the display one space to the left.
    display.print(Welcome2[positionCounter]); // Print a message to the LCD.
    delay(tim); //wait for 250 microseconds
  }
#endif

  gpsSerial.begin(9600);    // connect gps sensor
  secLastSync = 0l;
  
  display.clear();
  secDisplayOn = secLastButton = actSec();
  bDispOn = true;
  strcpy( cLocation, "*");  // '*' for automatic in function GetLocationName, else fix name

#ifdef DEBUG
  Serial.begin(9600);       // serial port for monitoring
#endif 

#ifdef AUTO_RESET
// Display set to off as default if AUTO_RESET
  #ifdef LED_MATRIX
    display.onoff(false);
  #else
    display.noBacklight();   //no backlight 
    brightness = 0;
  #endif       
  bDispOn = false;
#endif
}


/****** Main Loop ***************************************************/
void loop() 
{
  static int LastSec = second();

#ifdef LED_MATRIX
  #ifndef ARDU_MEGA
    display.rect(0,7,display.get_width()-1,8,0);
    for( short n=display.get_width()-1; n>display.get_width()-15; n-=6 )
      display.line(n,0,n,15,0);
  #endif
#endif

  CheckButtons();

#ifndef DEBUG
  //**** GPS?:
  if( bGPSrunning ) GetGPS(1);
  else if( actSec()>=secLastSync+SecGPSsync-15 )  // start port 15 secs before next sync
  { 
    gpsSerial.begin(9600);
    bGPSrunning = true;
  }
  //**** after 10 tries to sync with fail (no GPS) -> unsynched
  if( actSec()>=secLastSync+10*SecGPSsync && secLastSync>0l ) IsSynced = false;
#else
  IsSynced = true;
#endif
  
  CheckButtons();
  CheckDisplayOn();

  //**** now day? calc globals new:
  if( LastDay!=day() && gpsLon!=0.0 && gpsLat!=0.0)
  {
    double tjulv, tjuln;
    juldat( (double)TimeZone, day(), month(), year(), &tjulv, &tjuln, &DayOfWeek);
    Set_globals(tjulv);
    LastDay = day();
  }
  
  if( !bModeStatic )
  {
    //**** every 'SecAutoOn' (5) minutes automatic on:
    if( !bAutomatic && actSec()>secLastButton+SecAutoOn )
    {
      bAutomatic = true;
      nCount = 0;
      nDisplayOthers = 0;
#ifdef ARDU_MEGA
        nDisplaySunMoon = 0;
        nDisplayWeather = 0;
#endif
    }
  }

  //**** new second?:
  if( LastSec!=second() )
  {
    //**** time (second line too):
    Output_Time( 0, 0, hour(), minute(), second(), 0 );
    LastSec = second();
    Output_Blink(LastSec);
  }
  else
  {
    //**** get new weather data:
#ifdef WEATHER_BME     // get preasure?
    if(actSec()-LastPressSec>60 || pp_mean<-200)  // only every minute
    {
      press = dht_bme.readPressure();    // read air preasure
      if (!isnan(press) )
      { 
        press /=100.0F;
        if( pp_mean<-200 ) pp_mean = press;
        else pp_mean = (1.0-fAnteil)*pp_mean + fAnteil*press;
      }
      LastPressSec = actSec();
    }
    else
#endif
    if(actSec()-LastTempSec>5 || tt_mean<-200)
    {
      temp = dht_bme.readTemperature(); // read temperature
      //Serial.println(temp);
      if (!isnan(temp) )
      {
#ifdef VS
          // empirische Korrecktur:
          temp = 0.13 + 0.92*temp - 0.0048*temp*temp;
          fTempOffset = 0.0;
#endif        
        if( tt_mean<-200 ) tt_mean = temp;
        else tt_mean = (1.0-fAnteil)*tt_mean + fAnteil*temp;
        if( tt_mean<temp_min ) temp_min = tt_mean;
        if( tt_mean>temp_max ) temp_max = tt_mean;
      }
      LastTempSec = actSec();
    }
    else if(actSec()-LastHumSec>4 || hh_mean<-200)
    {
      hum = dht_bme.readHumidity();     // read humidity
      if (!isnan(hum) )
      {
        if( hh_mean<-200 ) hh_mean = hum;
        else hh_mean = (1.0-fAnteil)*hh_mean + fAnteil*hum;
        if( hh_mean<hum_min ) hum_min = hh_mean;
        if( hh_mean>hum_max ) hum_max = hh_mean;
      }
      LastHumSec = actSec();
    }
  }
}
/****** End Main Loop ***********************************************/


long actSec() {
  return (long)(millis()/1000l);
}

#ifdef LED_MATRIX
void AnimationOff() {
  byte ym=display.get_height()/2, ymax=display.get_height()-1;
  short j;

  for (j=-ym;j<display.get_width()+10; j++) 
  {
    display.line(j,   0,j+ym,ym,1);
    display.line(j,ymax,j+ym,ym,1);
    //display.line(j,0,j,ymax,1);
    if( j-10>=-ym )
    {
      display.line(j-10,   0,j-10+ym,ym,0);
      display.line(j-10,ymax,j-10+ym,ym,0);
      //display.line(j-10,0,j-10,ymax,0);
    }
    delay(35);
    //display.rect(xm-j,ymax-j,xm+j,ymax+j,0);
    //display.rect(xm-j-1,ymax-j,xm+j-1,ymax+j,0);
  }
  display.clear();
}
#endif


bool CheckDisplayOn() {  // return true if switched off
  if( bDispOn && SecDisplayOff>0l )
  {
    if( actSec()>secDisplayOn+SecDisplayOff )
    {
#ifdef LED_MATRIX
       AnimationOff();
       display.onoff(false);
#else
       display.noBacklight();   //no backlight 
       brightness = 0;
#endif       
       bDispOn = false;
       bModeStatic = false;
       return true;
    }
  }
  return false;
}


bool SetDisplayOn() {
  if( !bDispOn )
  {
#ifdef LED_MATRIX
    display.onoff(true);
#else
    display.setBacklight(5); //open the backlight 0..255
#endif 
    secDisplayOn = actSec();
    bDispOn = true; 
    double tjulv, tjuln;
    juldat( (double)TimeZone, day(), month(), year(), &tjulv, &tjuln, &DayOfWeek);
    Set_globals(tjulv);
    LastDay = day();

    return true;  
  }
  return false;
}

//**** get GPS data and display time:
void GetGPS(int DoDisplay)
{
  if( !bGPSrunning ) return;
  while((gpsSerial.available())>0)
  {
    if (gps.encode(gpsSerial.read())) Check_gps(DoDisplay);
  }  
}


//**** clear display:
void ClearDisplay()
{
   display.clear(); 
   Output_Time( 0, 0, hour(), minute(), second(), 1 );
   bHas2Clear = false; 
}



//**** switching the display?:
void Display_second()
{
  if( bModeStatic ) nCount = 0;   // no switching

  // Switch to next display?:
  if( nCount>=nMapper[nDisplayMode] && bAutomatic )
  {
    nCount = 0;
    nCntMinMax = 0;  // for display min/max temp., rise/set
    ShowTime = true;
    nDisplayMode++;
    if( bHas2Clear ) ClearDisplay();
    do
    {
      if( nMapper[nDisplayMode]==0 ) nDisplayMode++;
      if( nMapper[nDisplayMode]==-1 ) { nDisplayMode = 0; DoScrollingFirst = true; }
    } while(nMapper[nDisplayMode]<=0 );
  }
  //Serial.println(nDisplayMode);

  switch (nDisplayMode)
  {
    case  0: Output_Pos(0, 1); break;
    case  1: Output_Date(0, 1); break;
    case  2: Output_WeekDay(0, 1); break;
    case  3: Output_Clock(display.get_width()/2-1, display.get_height()/2, 7, hour(), minute() ); break;
    case  4: Output_UTC(0, 1); break;
    case  5: Output_JD(0, 1, day(), month(), year()); break;
    case  6: Output_Moz(0, 1); break;
    case  7: Output_Woz(0, 1); break;
    case  8: Output_ZGL( 0, 1); break;
    case  9: Output_Stz(0, 1); break;
    case 10: Output_Sun(0, 1); break;
    case 11: Output_SunRiseSet(0, 1, nMapper[11]); break;
    case 12: Output_MoonPhaseTxt(0, 1); break;
    case 13: Output_MoonPercent(0, 1); break;
    case 14: Output_Sensor( 0, 1, temp,   tt_mean, fTempOffset, 0); break;
    case 15: Output_TempMinMax( 0, 1, nMapper[15]); break;
    case 16: Output_Sensor( 0, 1, hum,    hh_mean, fHumOffset,  1); break;
    case 17: Output_HumMinMax( 0, 1, nMapper[17]); break;
    case 18:
#ifdef WEATHER_BME
             Output_Sensor( 0, 1, press,  pp_mean, fAirOffset,  2);
#endif
              break;
    case 19: if( Output_Text( TextRun1, TextRun2, nMapper[nDisplayMode] ) )
               nCount = nMapper[nDisplayMode]; 
             break;
    default: break;
  }
  if( bAutomatic ) nCount++;
}


bool CheckButtons()
{
  // check if a button is pressed.
  // if pressed the buttonState is LOW:
#ifdef ARDU_MEGA
  // two buttons together?
  if( digitalRead(BUTTON_1) == LOW && digitalRead(BUTTON_6) == LOW )  // new since 22.04.2021
  {
    long curmillis = millis();
    long timeout_msec = 700l;
    // pressed both:
    while( digitalRead(BUTTON_1)==LOW && digitalRead(BUTTON_6) == LOW && millis()-curmillis<timeout_msec );
    while( millis()-curmillis<timeout_msec );
    bModeStatic = !bModeStatic;
    bAutomatic  = !bModeStatic;
    if( bModeStatic)
    {
      for( nCount=display.get_width()-1; nCount>=0; nCount--)
      { display.plot(nCount,display.get_height()-1,1); delay(20); } // line right to left
    }
    else
    {
      for( nCount=0; nCount<display.get_width(); nCount++)
      { display.plot(nCount,display.get_height()-1,1); delay(20); } // line right to left
    }
    delay(1500); //wait for microsecond
    while( digitalRead(BUTTON_1)==LOW || digitalRead(BUTTON_6) == LOW );
    bHas2Clear = true;
    nCount = nCntMinMax = 0;
    return true;
  }
  else 
#endif  
  if (digitalRead(BUTTON_1) == LOW && digitalRead(BUTTON_6)!=LOW)
  {
    if( releaseButton(BUTTON_1) ) {
      //**** used if change brightness:
#ifdef LED_MATRIX
      brightness += 3;
      if( brightness>maxBrightness ) brightness = 0;
      byte i, y=display.get_height()/2+3;
      display.brightness(brightness);
      for( i=0; i*3<brightness; i++, y++ )
        display.line(display.get_width()-2-i,y, display.get_width()-1,y,1);
#endif
      return true;
    }
  }
  else if(digitalRead(BUTTON_2) == LOW && digitalRead(BUTTON_1)!=LOW) // UT
  {
    if( releaseButton(BUTTON_2) ) {
      bAutomatic = false;
      nCount = 0;
      nDisplayMode = FCT_BUTTON_2;
      Display_second();
      return true;
    }
  }
  else if(digitalRead(BUTTON_3) == LOW && digitalRead(BUTTON_1)!=LOW)  // MOZ
  {
    if( releaseButton(BUTTON_3) ) {
      bAutomatic = false;
      nCount = 0;
      nDisplayMode = FCT_BUTTON_3;
      Display_second();
      return true;
    }
  }
  else if(digitalRead(BUTTON_4) == LOW && digitalRead(BUTTON_1)!=LOW)  // WOZ
  {
    if( releaseButton(BUTTON_4) ) {
      bAutomatic = false;
      nCount = 0;
      nDisplayMode = FCT_BUTTON_4;
      Display_second();
      return true;
    }
  }
  else if(digitalRead(BUTTON_5) == LOW && digitalRead(BUTTON_1)!=LOW)  // STZ
  {
    if( releaseButton(BUTTON_5) ) {
      bAutomatic = false;
      nCount = 0;
      nDisplayMode = FCT_BUTTON_5;
      Display_second();
      return true;
    }
  }
  else if(digitalRead(BUTTON_6) == LOW && digitalRead(BUTTON_1)!=LOW)  // other times
  {
    if( releaseButton(BUTTON_6) ) {
      bAutomatic = false;
      nCount = 0;
      nDisplayMode = nMenuOthers[nDisplayOthers];
      nDisplayOthers++;
      if( bHas2Clear ) ClearDisplay();
      if(nMenuOthers[nDisplayOthers]==-1) nDisplayOthers = 0; 
      Display_second();
      return true;
    }
  }
#ifdef VS
  else if(digitalRead(BUTTON_7) == LOW && digitalRead(BUTTON_1)!=LOW)  // sun/moon
  {
    if( releaseButton(BUTTON_7) ) {
      bAutomatic = false;
      nCount = 0;
      nDisplayMode = nMenuSunMoon[nDisplaySunMoon];
      nDisplaySunMoon++;
      if(nMenuSunMoon[nDisplaySunMoon]==-1) nDisplaySunMoon = 0; 
      Display_second();
      return true;
    }
  }
  else if(digitalRead(BUTTON_8) == LOW && digitalRead(BUTTON_1)!=LOW)  // weather
  {
    if( releaseButton(BUTTON_8) ) {
      bAutomatic = false;
      nCount = 0;
      nDisplayMode = nMenuWeather[nDisplayWeather];
      nDisplayWeather++;
      if(nMenuWeather[nDisplayWeather]==-1) nDisplayWeather = 0; 
      Display_second();
      return true;
    }
  }
#endif

  return false;
}

//**** wait for button released, return value true if display is on (for switch on if false)
bool releaseButton(int Button)
{
  long curmillis = millis();
  long timeout_msec = 700l;
  
  // turn LED on:
  while( digitalRead(Button)==LOW && millis()-curmillis<timeout_msec );
  while( millis()-curmillis<timeout_msec );
  // button still pressed? -> switch off
  if( Button==BUTTON_1 && digitalRead(Button)==LOW && millis()-curmillis>=timeout_msec )
  {
    secDisplayOn = actSec() - SecDisplayOff-10;
#ifdef LED_MATRIX
    brightness -= 3;
#else
    brightness = brightness-1;
    //delay(1000);
#endif
    return( true );
  }

  secDisplayOn = secLastButton = actSec();
  return( !SetDisplayOn() );
}


void Check_gps(short DoDisplay)
{
  //**** output number Sync
  //display.setCursor(12,0);
  //sprintf(sBuf,"%4d ", AnzSync);
  //display.print(sBuf);
  int actDaySec, gpsDaySec;
  //Serial.print(".");
   
  //**** Position
  if (gps.location.isValid())
  {
    //gps.f_get_position(&gpsLat,&gpsLon); // get latitude and longitude
    gpsLat = gps.location.lat();   // Breite und Länge einlesen
    gpsLon = gps.location.lng();

    if (gps.date.isValid() && gps.date.age() < 2000 ) // valid date format as dd.mm.yyyy
    {
      int day_l, month_l, year_l, hour_l, minute_l, second_l;  // local vars
      //**** Time
      day_l   = gps.date.day();
      month_l = gps.date.month();
      year_l  = gps.date.year();
      if (gps.time.isValid() && gps.date.isValid() && gps.time.age() < 2000 && year_l>=year()) // valid time, format as hh:mm:ss
      {
        hour_l   = gps.time.hour();
        minute_l = gps.time.minute();
        second_l = gps.time.second();
        gpsDaySec = hour_l*3600 + minute_l*60 + second_l;
        if( DoDisplay>0 )
        {
          Output_Time( 0, 0, hour(), minute(), second(), 0 );
        }
        Output_Blink(second());
        //Serial.print(F("Time: "));
        //sprintf(sBuf, "%02d.%02d.%d %02d:%02d:%02d", day_l, month_l, imod(year_l,100), hour_l, minute_l, second_l );
        //Serial.println(sBuf);
        actDaySec = hour()*3600 + minute()*60 + second();
        // have to sync?
        if( ((actDaySec-gpsDaySec)>2 || (actDaySec-gpsDaySec)<-2 ) && actSec()>=secLastSync+SecGPSsync ) IsSynced = false;
        if( actSec()<secLastSync+SecGPSsync && IsSynced && secLastSync>0l ) return;
        
        IsSynced = true;
        AnzSync++;
        secLastSync = actSec();
        bGPSrunning = false;
        gpsSerial.end();
        if( second_l<59 ) second_l++; // data comes before
        delay(300);
 
        setTime(hour_l, minute_l, second_l, day_l, month_l, year_l);  // HH,MM,SS, Day, Month, Year
        if( DoDisplay>0 )
        {
          display.setCursor(0,8);
          display.print(F(" *"));
        }
      }
    }
  }
  else
  {
    display.setCursor(0,0);
    display.print(F("GPS?..     "));
  }
}


//**** calc aprox. distance [km] between two positions [°]:
#ifdef ARDU_MEGA
double calcDist( double TestLat, double TestLon )
{
  double dLat=0.0, dLon=0.0;

  dLat = ((double)gpsLat-TestLat);
  dLon = (gpsLon-TestLon) * cos(rho*gpsLat);
  return sqrt( dLat*dLat + dLon*dLon )*111.0; 
  //return fabs(1.0);
} 


const char *GetLocationName()
{
  //return "Hannover  \0";
  if(      calcDist( 52.35, 9.75 ) < 20.0 ) return "Hannover  ";
  else if( calcDist( 52.15, 9.95 ) < 10.0 ) return "Hildesheim";
  else 
  return "?         ";
} 
#endif

//**** set global vars (every day):
void Set_globals(double dJD)
{
  double   t, l, m, o, ll, mm, denut;
  int iwt;

  //**** Nutationglieder berechnen
  t  = (dJD-2451545.0)/36525.0;
  l  = b2pi(rho*(218.316+481267.881*t));
  m  = b2pi(rho*(134.963+477198.867*t));
  o  = b2pi(rho*(125.045-  1934.136*t));
  ll = b2pi(rho*(280.466+ 36000.770*t));
  mm = b2pi(rho*(357.528+ 35999.050*t));
  dlnut = rho/3600.*(-17.2*sin(o) +0.206*sin(o*2)-1.319*sin(ll*2)+
                  0.143*sin(mm)-0.227*sin(2*l)+0.071*sin(m));
  denut = rho/3600.*(9.203*cos(o) - 0.09*cos(o*2)+0.574*cos(ll*2)+
                  0.022*cos(ll*2+mm)+0.098*cos(l*2)+0.020*cos(l*2-o));

  //**** Wahre Schiefe der Ekliptik berechnen:
  t   = (dJD-2415020.0)/36525.;
  eps = (23.452294440-0.01301250*t)*rho+denut;

  //**** calculate start of summertime:
  Doy = doy(year(),month(),day(), &iwt);
  NowDayTime = false;
  if( WithDayTime )
  {
    BeginSummerTime = doy(year(), 4, 1, &iwt);  
    BeginSummerTime -= iwt+1;                    // last sunday march (0=Mo)
    EndSummerTime = doy(year(),11, 1, &iwt);     
    EndSummerTime -= iwt+1;                      // last sunday october
    //**** now in summertime?
    if( Doy>=BeginSummerTime && Doy<EndSummerTime ) NowDayTime = true;
  }
  
  if( !bModeStatic ) bAutomatic = true;
}


/*********************************************************************/
void juldat( double time, int iday, int imonth,
             int iyear, double *tjulv, double *tjuln, int *iwt)
{
/*********** Berechnung von Datum <-> jul.-Datum
               time:    zeit [h, ut]       iday:    tag
               imonth:  monat              iyear:   jahr
               tjul:    jul.-datum         iwt:     wochentag, mo=0,...

  Übergang julianischer zu gregorianischer Kalender berücksichtigt! */

  long    ib; // ia, ic, id, ie, ig;
  int     iy, im, igreg;

//********** Wandlung Datum -> julianisches Datum:
  iy = iyear;
  im = imonth;
  if( imonth<=2l ) { iy = iyear-1; im = imonth+12; }
  igreg = 1;
  ib = -2;
  //if( iyear<1582 || (iyear==1582 && imonth<10) ||
  //   (iyear==1582 && imonth==10 && iday<5) ) igreg = 0;
  if( igreg==1 ) ib = ((int)((double)iy)/400)-((int)((double)iy)/100);
  *tjulv = ((double)(long)(365.25*(double)iy)) +
          ((double)(long)(30.6001*(double)(im+1))) +
          ib + 1720996.0 + iday;
  *tjuln = 0.5+time/24.0;
  if( *tjuln>=1.0 )
  {
    *tjuln = *tjuln-1.0;
    *tjulv = *tjulv+1.0;
  }
  *iwt  = (int)(fmod((*tjulv+*tjuln+0.5),7.0));
}


//********************************************************************
// calculate sederial time [h]
double stz( double tjulv, double tjuln )
{
  double   lmst, t0, gmst0, ut;

//********* mean siderial time:
  t0    = ( (double)(long)(tjulv-2415000.0+tjuln-0.5)-19.5 )/36525.0;
  gmst0 = b24( 6.6460656+2400.051262*t0+0.00002581*t0*t0 );
  ut    = b24( 24.*(tjuln-0.5) );            // Time in UT
  lmst  = gmst0+1.0027379093*ut+gpsLon/15.0;  

//********* true siderial time:
  return b24( lmst+dlnut*12./PI*cos(eps));
}

#ifdef AUTO_RESET
void Reset()
{
  asm volatile("jmp 0");
}
#endif

void PrintTime(int hh, int mm, int ss, char cSepa, bool bSmall, bool bBlinkDP)
{
  if (bSmall)
  {
    #ifdef BLINK_DP
      if( bBlinkDP && second() % 2==1) cSepa = ' '; 
    #endif
    display.set_font(5, 7, false);
    sprintf(sBuf, "%02d", hh);
    display.print(sBuf);
    // Separator
    PrintSeparator(cSepa);
    // MM
    sprintf(sBuf, "%02d", mm);
    display.print(sBuf);
    // Separator
    PrintSeparator(cSepa);
    // SS
    sprintf(sBuf, "%02d", ss);
    display.print(sBuf);
  }
  else 
  {
    display.set_font(5, 7, false);
    sprintf(sBuf, "%02d%c%02d%c%02d    ", hh, cSepa, mm, cSepa, ss );
    sBuf[maxCharsLine] = 0;
    display.print(sBuf);
  }
}

void PrintSeparator(char cSepa)
{
  if( cSepa==':' || cSepa==' ')
  {  
    display.set_font(5,7, true);
    display.printByte(cSepa);
    if( cSepa==' ') display._x -= 2;
  }
  else if( cSepa=='.' )
  {
    display.set_font(5,7, true);
    display.printByte(' ');
    display._x -= 4;
    display.set_font(3,5, true);    
    display._y += 3;
    display.printByte('.');
    display._y -= 3;
  }
  display.set_font(5,7, false);
}


//*** correction timezone for hour:
int CorrTimeZone(int hh)
{
  hh += TimeZone;
  if( NowDayTime ) hh++;
  if( hh>23 ) { hh -= 24; }
  return hh;
}

//**** Output time in first line:
void Output_Time(int x, int y,  int hh, int mm, int ss, short DoIt, char cSepa)
{
  static short dots=0;
  int dd, mon, yy;

  display.setCursor(x,y);
  if( IsSynced )
  {
    hh = CorrTimeZone(hh);

    if( LastSecTime!=ss || DoIt==1 )
    {
      // show nothing?
      if( ShowTime || DoIt==1 )
      {
#ifdef LED_MATRIX
  #ifdef VS     // output HH:MM:SS
        PrintTime(hh, mm, ss, cSepa, false, true );  // Time
        sBuf[maxCharsLine] = 0;
        display.print(sBuf);
  #else         // output HH:MM:SSdd
        dd=day();
        mon=month();
        yy=year();
        if( (NowDayTime && hour()+TimeZone>22) || (!NowDayTime && hour()+TimeZone>23) )  // hour() is in UTC
        {
          dd++;
          if( dd>NumberDaysMonth[mon-1] ) { dd = 1; mon++; }
          if( mon>12 ) { dd = 1; mon = 1; yy++; }
  }
        PrintTime(hh, mm, ss, cSepa, true, true );  // Time
        // flickers: display.printByte(' '); display._x -= 5;
        display.line(display._x,15,display.get_width()-1,15,0);
        display.space(1);
        display.set_font(3, 5, false);
        sprintf(sBuf, "%02d", dd);
        display.print(sBuf);
        display.set_font(5, 7, false);
  #endif
#else
        PrintTime(hh, mm, ss, cSepa, false, false );  // Time
        sBuf[maxCharsLine] = 0;
        display.print((char*)sBuf);
#endif
#ifdef DEBUG
  Serial.print(sBuf); Serial.print(" * "); Serial.println(nDisplayMode); 
#endif
      }
      LastSecTime = ss;
      //**** Blink and output in second line:
      Display_second();
      // reset min or max temperature or Reset at 04:00:00 local?
      if( hh==4 && mm==0 && ss==0 )
      { 
#ifdef AUTO_RESET
        Reset();
#else
        temp_max = hum_max = -100.0;
        temp_min = hum_min =  100.0;
#endif
      }
      #ifdef VS
        // switch display on Di and Do 20:00:00:
        if( hh==20 && mm==0 && ss==0 && (DayOfWeek==1 || DayOfWeek==3) ) SetDisplayOn();
      #else 
        // test for switching on/off
        // Mo-Fr 07:00, Sa, So 09:00 on
        if( hh== 7 && mm==0 && ss==0 && (DayOfWeek>=0 && DayOfWeek<=4) ) SetDisplayOn(); // Mo-Fr
        if( hh== 9 && mm==0 && ss==0 && (DayOfWeek==5 || DayOfWeek==6) ) SetDisplayOn(); // Sa,So
        // So,Mo-Do 23:00, Sa,So 01:00 off
        if( hh==23 && mm==30 && ss==0 && ((DayOfWeek>=0 && DayOfWeek<=3) || DayOfWeek==6 )) secDisplayOn = actSec() - SecDisplayOff-10;
        if( hh== 1 && mm== 0 && ss==0 && (DayOfWeek==5 || DayOfWeek==6)) secDisplayOn = actSec() - SecDisplayOff-10;
      #endif
    }
  }
  else if( ShowTime )
  {
    dots++;
    if( dots==1 )      display.print(F("GPS?.    "));
    else if( dots==2 ) display.print(F("GPS?..   "));
    else             { display.print(F("GPS?...  ")); dots = 0; }
  }
}


void Output_Blink(int sec)
{
#ifdef BLINK
  static int Last_ss = -1;
  
  if( IsSynced && Last_ss!=sec )
  {
    digitalWrite(LED_RED,HIGH); 
    delay(100);
    digitalWrite(LED_RED,LOW); 
    Last_ss = sec;
  }
#endif
}


//**** Output position lat, lon [°]:
void Output_Pos(int x, int y)
{
  int hh, mm, vz;
  double dmm;
  
  ShowTime = true;
  if( IsSynced )
  {
    display.setCursor(x,y);
    display.set_font(4,7,false);
    dm(gpsLat, &hh, &dmm, &vz );
    if( maxCharsLine>8 && Displays!=2 )
      { dtostrf(dmm, 4, 1, sfloat); sprintf(sBuf,"%d%c%s  ", hh, CHAR_DEG, sfloat ); }
    else
      { mm = (int)(dmm+0.5); sprintf(sBuf,"%d%c%02d    ", hh, CHAR_DEG, mm ); }
    dm(gpsLon, &hh, &dmm, &vz );
    if( maxCharsLine>8 && Displays!=2 )
      { dtostrf(dmm, 4, 1, sfloat); sprintf(sBuf+7,"/%d%c%s     ", hh, CHAR_DEG, sfloat ); }
    else
      { mm = (int)(dmm+0.5); sprintf(sBuf+5, "/%d%c%02d        ", hh, CHAR_DEG, mm ); }
 #ifdef LED_MATRIX
    display.print(sBuf);
    //display.rect(display.get_width()-2,0,display.get_width()-1,display.get_height()-1,0);
    display.set_font(5,7,false);
 #else
    sBuf[maxCharsLine] = 0;
    display.print((char*)sBuf);
 #endif
  }
}


void Output_Date(int x, int y)
{
  double tjulv, tjuln;
  int dd, mm, yy;

  ShowTime = true;
  dd=day();
  mm=month();
  yy=year();
  if( (NowDayTime && hour()+TimeZone>22) || (!NowDayTime && hour()+TimeZone>23) )  // hour() is in UTC
  {
    dd++;
    if( dd>NumberDaysMonth[mm-1] ) { dd = 1; mm++; }
    if( mm>12 ) { dd = 1; mm = 1; yy++; }
  }
  // determine day of week:
  juldat( 0.0, dd, mm, yy, &tjulv, &tjuln, &DayOfWeek);
  
  if( Displays==2 )
  {
    //display.set_font(4,7,false);
    //sprintf(sBuf, " %02d.%02d.%04d  ", dd, mm, yy );
    display.set_font(5,7,true);
    sprintf(sBuf, "_ %02d.%02d.%02d    ", dd, mm, imod(yy,100) );
  }
  else
    sprintf(sBuf, "%02d.%02d.%02d   ", dd, mm, imod(yy,100) );
  if( Displays>2 && DayOfWeek>=0 && DayOfWeek<7 )
    sprintf(sBuf+8, ", %s", DayShort[DayOfWeek]);
  display.setCursor(x,y);
#ifdef LED_MATRIX
  //#ifndef VS
  //  display.set_font(5,7,true);
  //#endif
  if( DoScrollingFirst )
  {
    display.scrolltext_to(0,0,sBuf, 35, 1, 2); // end, x, text, delay, times, dir
    DoScrollingFirst = false;
    //display.setCursor(x,12);
  }
  else
  {
    display.print(sBuf);
#else
  {
    display.print((char*)sBuf);
#endif
    display.print(F("       "));
  }
  display.set_font(5,7,false);
}


void Output_WeekDay(int x, int y)
{
  double tjulv, tjuln;
  int dd, mm, yy;

  ShowTime = true;
  dd=day();
  mm=month();
  yy=year();
  if( (NowDayTime && hour()+TimeZone>22) || (!NowDayTime && hour()+TimeZone>23) )  // hour() is in UTC
  {
    dd++;
    if( dd>NumberDaysMonth[mm-1] ) { dd = 1; mm++; }
    if( mm>12 ) { dd = 1; mm = 1; yy++; }
  }
  // determine day of week:
  juldat( 0.0, dd, mm, yy, &tjulv, &tjuln, &DayOfWeek);
  
  if( Display_x>=8 && DayOfWeek>=0 && DayOfWeek<7 )
  { 
    display.setCursor(x,y);
    sprintf(sBuf, "%s           ", DayLong[DayOfWeek]+2 );
#ifdef LED_MATRIX
    if( Displays==2 && DayOfWeek==3 ) display.set_font(4,7,false);
    else                              display.set_font(5,7,true);
    // "  Freitag":
    if( Displays==2 ) display.space(atoi(DayLong[DayOfWeek]));  // to center text
    if( DayOfWeek==4 )
    {
      display.print(F("F"));
      display._x-=1;
      display.print(sBuf+1);
    }
    else
      display.print(sBuf);
    if( Displays>2 ) display.rect(display.get_width()-2,0,display.get_width()-1,display.get_height()-1,0);
    display.set_font(5,7,false);
#else
    sBuf[maxCharsLine] = 0;
    display.print((char*)sBuf);
#endif
  }
}


void Output_JD(int x, int y, int dd, int mm, int yy)
{
  int iday=day();
  int imonth = month();
  int iyear = year();
  int iwt;
  double time = (double)hour()+(double)minute()/60.0+(double)second()/3600.0;
  double tjulv, tjuln;    // Vor-, Nachkomma Stellen
  char sBufNach[16], *pPoint;

  ShowTime = true;
  juldat( time, iday, imonth, iyear, &tjulv, &tjuln, &iwt);

  // auf 13 Stellen anzeigen
  dtostrf(tjulv, 8, 1, sfloat);
#ifdef DEBUG
  Serial.print("Output_JD: "); Serial.println(sfloat);
#endif
  //String str = String(ModJulianDate(yy, mm, dd), 1);
  //String strv = String(tjulv, 1);
  //String strn = String(tjuln, 6);
  sprintf(sBuf, "%s           ", sfloat);
  pPoint = strchr(sBuf, '.');
  dtostrf(tjuln, 8, 6, sfloat);
  sprintf(sBufNach, "%s   ", sfloat );
  if( Displays==2 ) sBufNach[6] = 0;
  strcpy(pPoint+1, sBufNach+2);
  display.setCursor(x,y);
#ifdef LED_MATRIX
  display.set_font(4,7,false);
  if( Displays==2 ) display.print(sBuf+2);
  else              display.print(sBuf);
  display.set_font(5,7,false);
#else
  sBuf[maxCharsLine] = 0;
  display.print((char*)sBuf);
#endif
}


//**** Output UT in second line:
void Output_UTC(int x, int y)
{
  static int LastSec = -1;
  int hh, mm, ss, vz;
  double ds;

  ShowTime = true;
  display.setCursor(x,y);
  if( IsSynced )
  {
    double dUT = (double)hour() + minute()/60.0 + second()/3600.0;
    dUT += 1.0/3600.0;   // sync to Time

    dms(b24(dUT), &hh, &mm, &ds, &vz );
    ss = (int)ds;
    if( ss==LastSecTime+1 ) ss = LastSecTime;  // same sec like normal time

    sprintf(sBuf, "%02du%02d:%02d UTC   ", hh, mm, ss );
    sBuf[maxCharsLine] = 0;
    if( ss!=LastSec )
    {
#ifdef LED_MATRIX
  #ifdef VS
      display.print(sBuf);
  #else
      PrintTime(hh, mm, ss, SEPARATOR, true, false);  // UT
      display.line(display._x,7,display.get_width()-1,7,0);
      display.space(1);
      display.set_font(3, 5, false);
      display.print(F("UT"));
      display.set_font(5, 7, false);
  #endif
#else
      display.print((char*)sBuf);
#endif
      LastSec = ss;
    }
  }
  else
  {
    display.print(F("##u##:##"));
  }
}


//**** Output of analog clock, center x, y, rarius r:
void Output_Clock(int x, int y, int r, int hh, int mm)
{
  static double LastTime = -1.0;
  double dTime, wh, wm;

#ifdef ARDU_MEGA
    hh = CorrTimeZone(hh);
 
    dTime = (double)hh + 1.0/60.0*mm;         // time in [h]
    if( dTime>12.0 ) dTime-=12.0;             // 0..12
    if(     dTime>11.6 || dTime<0.4 ) dTime-= 0.183;  // -11 min for show hour correctly)
    else if(dTime> 2.6 && dTime<3.4 ) dTime+= 0.183;  
    else if(dTime> 5.6 && dTime<6.4 ) dTime+= 0.183;  
    else if(dTime> 8.4 && dTime<9.4 ) dTime-= 0.183;  
    if( ShowTime || LastTime!=dTime )         // plot new?
    {
      display.clear();
    } 
    ShowTime = false; 
    bHas2Clear = true;
    // print clock:
    display.circle(x,y,r,1);
    /*display.plot(x+r-1,y,1);
    display.plot(x-r+1,y,1);
    display.plot(x,y+r-1,1);
    display.plot(x,y-r+1,1); */
    wh = pi2*dTime/12.0;
    wm = pi2*mm/60.0;
    //r -= 1;
    // minutes:
    display.line(x,y, x+(int)(sin(wm)*r+0.5),y+(int)(cos(wm)*r+0.5),1);
    // hour:
    r -= 2;
    display.line(x,y, x+(int)(sin(wh)*r+0.5),y+(int)(cos(wh)*r+0.5),1);
    //display.plot(x,y,0);
#ifdef DEBUG
//  dtostrf(wh, 8, 3, sBuf);
  Serial.print("Output_Clock: h="); 
  Serial.print(hh);
  Serial.print("  m="); 
  Serial.print(mm);
  Serial.println("");
#endif
    
    //display.setCursor(0,0);
    //display.set_font(5,7,true);
    LastTime = dTime;
#else
  ShowTime = true;
#endif

}


//**** Output mean local time in second line:
void Output_Moz(int x, int y)
{
  static int LastSec = -1;
  int hh, mm, ss, vz;
  double ds;

#ifdef ARDU_MEGA
  if( cLocation[0]=='*' || cLocation[0]!=0 )
  {
    ShowTime = false;
    display.setCursor(0,0);
    display.set_font(5,7,true);
    if( cLocation[0]=='*' ) display.print((char*)GetLocationName());
    else                    display.print(cLocation);
    display.print(F("         "));
    display.set_font(5,7,false);
  }
#else
  ShowTime = true;
#endif
    
  display.setCursor(x,y);
  if( IsSynced )
  {
    double dUT = (double)hour() + minute()/60.0 + second()/3600.0;
    dUT += gpsLon/15.0;

    dms(b24(dUT), &hh, &mm, &ds, &vz );
    ss = (int)ds;

    if( ss!=LastSec )
    {
      sprintf(sBuf, "%02do%02d:%02d MOZ        ", hh, mm, ss );
      sBuf[maxCharsLine] = 0;
#ifdef LED_MATRIX
  #ifdef VS
      display.print(sBuf);
  #else
      PrintTime(hh, mm, ss, SEPARATOR, true, false);  // MOZ
      display.line(display._x,7,display.get_width()-1,7,0);
      display.space(1);
      display.set_font(3, 5, false);
      display.print(F("OZ"));
      display.set_font(5, 7, false);
  #endif
#else
      display.print((char*)sBuf);
#endif
      LastSec = ss;
    }
  }
  else
  {
    display.print(F("##:##:##"));
  }
}


//**** calc ZGL [h]:
double ZGL(short year, short month, short day, short UT )
{
  double T;
  int iwt;
  
  T = doy(year, month, day, &iwt)+0.041666667*UT;
  return -0.171*sin(0.0337 * T + 0.465) - 0.1299*sin(0.01787 * T - 0.168);
}


//**** Output true local time in second line:
void Output_Woz(int x, int y)
{
 // WOZ = MOZ -0.171*sin(0.0337 * T + 0.465) - 0.1299*sin(0.01787 * T - 0.168)
 // T bezeichnet hier die Nummer des jeweiligen Tages. Für den 1. januar ist dies die 1, 
 // für den 2. Januar die 2, usw. Das angezeigte Ergebnis gibt die Zeit in Stunden an.  
  static int LastSec = -1;
  int hh, mm, ss, vz;
  double ds;

  display.setCursor(x,y);
  if( IsSynced )
  {
    if( !ShowTime )
    {
      ShowTime = true;
      Output_Time(0,0, hour(), minute(), second(), 1);
      LastSec = -1;
    }
    
    double dUT = (double)hour() + minute()/60.0 + second()/3600.0;
    dUT += gpsLon/15.0;
    dUT += ZGL(year(), month(), day(), hour());

    dms(b24(dUT), &hh, &mm, &ds, &vz );
    ss = (int)ds;

    if( LastSec!=ss )
    {
      sprintf(sBuf, "%02d%c%02d:%02d WOZ      ", hh, SEPARATOR, mm, ss );
      sBuf[maxCharsLine] = 0;
#ifdef LED_MATRIX
  #ifdef VS
      display.print(sBuf);
  #else
      PrintTime(hh, mm, ss, SEPARATOR, true, false);  // WOZ, Sun
      display.space(1);
      display.set_font(7,7,false);
      display.printByte(CHAR_SUN);
      display.set_font(5,7,false);
      display.printByte(' ');
  #endif
#else
      display.print((char*)sBuf);
#endif
      LastSec = ss;
    }
  }
  else
  {
    display.print(F("##:##:##"));
  }
}


//**** Output ZGL = WOZ - MOZ in hh:ss in second line:
void Output_ZGL(int x, int y)
{
 // WOZ = MOZ -0.171*sin(0.0337 * T + 0.465) - 0.1299*sin(0.01787 * T - 0.168)
 // T bezeichnet hier die Nummer des jeweiligen Tages. Für den 1. januar ist dies die 1, 
 // für den 2. Januar die 2, usw. Das angezeigte Ergebnis gibt die Zeit in Stunden an.  
  int hh, mm, ss, vz;
  double ds;

  ShowTime = true;
  display.setCursor(x,y);
  //display.set_font(5,7,true);
  if( IsSynced )
  {
    dms(ZGL(year(), month(), day(), hour()), &hh, &mm, &ds, &vz );
    ss = (int)ds;

    sprintf(sBuf, "  %c%2d:%02d ZGL    ", vz<0?'-':'+', mm, ss );
    sBuf[maxCharsLine] = 0;
#ifdef LED_MATRIX
    display.print(sBuf);
#else
    display.print((char*)sBuf);
#endif
  }
  else
  {
    display.print(F("  ##m ##s ZGL"));
  }
  display.set_font(5,7,false);
}


//**** Output siderial time in second line:
void Output_Stz(int x, int y)
{
  static int LastSec = -1;
  int hh, mm, ss, vz;
  double ds;

  ShowTime = true;
  display.setCursor(x,y);
  if( IsSynced )
  {
    int iday=day();
    int imonth = month();
    int iyear = year();
    int iwt;
    double time = (double)hour()+(double)minute()/60.0+(double)second()/3600.0;
    double tjulv, tjuln;    // Vor-, Nachkomma Stellen
    //char sBufNach[16], *pPoint;

    juldat( time, iday, imonth, iyear, &tjulv, &tjuln, &iwt);
    dms(stz(tjulv,tjuln), &hh, &mm, &ds, &vz );
    ss = (int)ds;

    //sprintf(sBuf, " Zeit  %02d:%02d:%02d", hh, mm, ss );
    sprintf(sBuf, "%02d*%02d:%02d STZ        ", hh, mm, ss );
    sBuf[maxCharsLine] = 0;
    if( LastSec!=ss )
    {
#ifdef LED_MATRIX
  #ifdef VS
      display.print(sBuf);
  #else
      PrintTime(hh, mm, ss, SEPARATOR, true, false);  // STZ
      display.space(1);
      display.set_font(7,7,false);
      display.printByte(CHAR_STAR);
      display.set_font(5,7,false);
      display.printByte(' ');
  #endif
#else
      display.print((char*)sBuf);
#endif
      LastSec = ss;
      //Serial.println(sBuf);
    }
  }
  else
  {
    display.print(F("##*##:##"));
  }
}


//********* Integer like in Basic (mathematic!) **********************
double basint( double x )
{
  double y;

  y = (double)(long) x;
  if( x<0.0 && x!=y ) y -= 1.0;
  return y;
}


//********* set intervall 0..2*Pi ************************************
double b2pi( double x )
{
  return ( x-pi2*basint( x*0.5/PI ) );
}


//********* set intervall 0..24 **************************************
double b24( double x )
{
  return ( x-24.0*basint( x/24.0 ) );
}


//**** Astronomy *****************************************************

// **** calc phase of moon:
// returns: 0=new moon, 3=first, 6=full, 9=last
int moon_phase()
{
  // calculates the age of the moon phase(0 to 7)
  // there are eight stages, 0 is full moon and 4 is a new moon
  double jd = 0; // Julian Date
  int b= 0;
  jd = ModJulianDate(year(), month(), day(), hour());
  jd = jd - ModJulianDate(2018,12,22,18);// used to debug this is a full moon
  jd /= SynMonth;                        // divide by the synodic moon cycles 0..1.0..2.0
  b = (int)jd;
  jd -= b; // leaves the fractional part of jd since new moon (0.0..0.99999)
  //nfm = String((int(SynMonth - ed))); // days to next full moon
  b = (jd*11.999999+0.5);               // 0.5 .. 11.4999 -> 0 .. 11 (rounded)
  b = b % 12;                           // 0 .. 11 
#ifdef DEBUG
  double ed = 0; // days elapsed since start of full moon
  ed = jd * SynMonth;                   // days elapsed since full moon
   Serial.print("moon_phase: MJD="); Serial.println(ModJulianDate(year(), month(), day(), hour()));
// Serial.print("  full:     "); Serial.println(ModJulianDate(2009,12,31,19));
   Serial.print(ed);
   Serial.print(", Umlauf%="); Serial.print(jd*100.0);
   Serial.print(", Sym="); Serial.println(b);
#endif
  return b;
}


//**** Prozent des Mondes:
int moon_percent()
{
  // calculates the age of the moon phase(0 to 7)
  // there are eight stages, 0 is full moon and 4 is a new moon
  double jd = 0; // Julian Date
  double ed = 0; // days elapsed since start of full moon
  int b = 0;
  jd = ModJulianDate(year(), month(), day(), hour());
  jd = jd - ModJulianDate(2018,12,22,18);  // used to debug this is a full moon
  jd /= SynMonth;                          // divide by the moon cycle
  b = (int)jd;
  jd -= b; // leaves the fractional part of jd
  ed = jd * SynMonth;                    // days elapsed this month
  //nfm = String((int(SynMonth - ed)));  // days to next full moon
  double DaysNextFull, dPhase;
  DaysNextFull = SynMonth - ed;          // days to next full moon
  if( DaysNextFull>SynMonth/2.0 ) 
  {
    dPhase = (DaysNextFull-SynMonth/2.0)/SynMonth*2.0*PI;  // Abnehmend
#ifdef DEBUG
    Serial.print("moon_percent: Phase="); Serial.print(-dPhase/rho);
    Serial.print("deg, "); Serial.print(-int(100.0-100.0*(1.0+cos(dPhase))/2.0+0.5));
    Serial.println("%");
#endif 
    return -int(100.0-100.0*(1.0+cos(dPhase))/2.0+0.5);
  }
  else
  {
    dPhase = (SynMonth/2.0-DaysNextFull)/SynMonth*2.0*PI;  // Zunehmend
#ifdef DEBUG
    Serial.print("moon_percent: Phase="); Serial.print(dPhase/rho);
    Serial.print("deg, "); Serial.print(int(100.0-100.0*(1.0+cos(dPhase))/2.0+0.5));
    Serial.println("%");
#endif 
    return int(100.0-100.0*(1.0+cos(dPhase))/2.0+0.5);
  } 
}


//**** calc day of the year:
int doy(int y, int m, int d, int *wt)
{
  double JD1, JDakt, jd2;
  int iwt;
  
  juldat(0.0, 1, 1, y, &JD1, &jd2, &iwt);
  juldat(0.0, d, m, y, &JDakt, &jd2, &iwt);
  *wt = iwt;
  return (int)(JDakt-JD1+1);
}


//**** modified julian date = JD - 2400000.5:
double ModJulianDate(int y, int m, int d, int ut)
{
  double jdv, jdn;
  int wt;

  juldat( ut, d, m, y, &jdv, &jdn, &wt);
  // -2400000.5
  jdv -= 2400000.5;
  return jdv+jdn;
}


void Output_MoonPhaseTxt(int x, int y)
{
  // 0=Voll, 1=Ab, 2=Letzte Viertel, 3=Sichel, 4=Neu, 5=Sichel, 6=Erstes viertel, 7=Zunehmend
  short nPhase = moon_phase();
  ShowTime = true;
  display.setCursor(x,y);
  if( Displays!=2 )
  {
    display.set_font(7,7,false);
    switch(nPhase)
    {
      case  0: display.printByte((byte)CHAR_MOON100); break;
      case  1: display.printByte((byte)CHAR_MOON83M); break;
      case  2: display.printByte((byte)CHAR_MOON67M); break;
      case  3: display.printByte((byte)CHAR_MOON50M); break;
      case  4: display.printByte((byte)CHAR_MOON33M); break;
      case  5: display.printByte((byte)CHAR_MOON17M); break;
      case  6: display.printByte((byte)CHAR_MOON0);   break;
      case  7: display.printByte((byte)CHAR_MOON17P); break;
      case  8: display.printByte((byte)CHAR_MOON33P); break;
      case  9: display.printByte((byte)CHAR_MOON50P); break;
      case 10: display.printByte((byte)CHAR_MOON67P); break;
      case 11: display.printByte((byte)CHAR_MOON83P); break;
    }
  }
  display.set_font(4,7,false);
  if( Displays>2 ) display.print(F(" "));
#ifdef LED_MATRIX
  if( Displays==2 ) display.space(2);
  display.set_font(5,7,true);
  //if( Displays==2 /*&& nPhase!=11 && nPhase!=0 && nPhase!=1 && nPhase!=5 && nPhase!=6 && nPhase!=7*/ ) display.set_font(4,7,false);
  switch(nPhase)
  {
    case  0: display.print(F(" Vollmond     ")); break;
    case  1: display.print(F("<Vollmond     ")); break;
    case  2: display.print(F(">3.Viertel    ")); break;
    case  3: display.print(F(" 3.Viertel    ")); break;
    case  4: display.print(F("<3.Viertel    ")); break;
    case  5: display.print(F(">Neumond      ")); break;
    case  6: display.print(F(" Neumond      ")); break;
    case  7: display.print(F(">Neumond      ")); break;
    case  8: display.print(F("<1.Viertel    ")); break;
    case  9: display.print(F(" 1.Viertel    ")); break;
    case 10: display.print(F(">1.Viertel    ")); break;
    case 11: display.print(F("<Vollmond     ")); break;
  }
  display.set_font(5,7,false);
#else
  display.print(F(" "));
  switch(nPhase)
  {
    case  0: display.print(F("Vollmond       ") ); break;
    case  1: display.print(F("nach Vollmond  ") ); break;
    case  2: display.print(F("vor 3. Viertel ") ); break;
    case  3: display.print(F("3. Viertel     ") ); break;
    case  4: display.print(F("nach 3.Viertel ") ); break;
    case  5: display.print(F("abneh. Sichel  ") ); break;
    case  6: display.print(F("Neumond        ") ); break;
    case  7: display.print(F("zuneh. Sichel  ") ); break;
    case  8: display.print(F("vor 1. Viertel ") ); break;
    case  9: display.print(F("1. Viertel     ") ); break;
    case 10: display.print(F("nach 1.Viertel ") ); break;
    case 11: display.print(F("vor Vollmond   ") ); break;
  }
#endif
}

void Output_Sun(int x, int y)
{
  static float LastElev = -99.0;
  byte cUpDown = ' ';
  bool bWithRiseSet = true;

  if( Displays==2 ) bWithRiseSet = false;
  ShowTime = true;
  if( IsSynced )
  {
    sundata Sun=sundata(gpsLat, gpsLon, 0);
    Sun.time(year(),month(),day(),hour(),minute(),second());  // UTC
    Sun.calculations();
    float rise, set, Tnow, elev;
    double mm;
    int hh, iv;
    elev = Sun.elevation_deg();
    rise = Sun.sunrise_time();
    set = Sun.sunset_time();
    if( isnan(rise) || isnan(set) ) bWithRiseSet = false;  // no rise/set times?
    
    Tnow = (float)hour()+1.0/60.0*minute();
    if( Tnow>set && bWithRiseSet )
    {
      // calc sun rise for next day:
      Sun.time(year(),month(),day()+1,hour(),minute(),second());  // UTC
      Sun.calculations();
      rise = Sun.sunrise_time();
    }

    display.setCursor(x,y);
    if( Displays==2 ) display.print(F(" "));
    display.set_font(7,7,false);
    display.printByte(CHAR_SUN);
    display.set_font(4,7,false);
    if( Displays==2 ) display.print(F(" "));
    if( elev>=0.0 )
      sprintf(sBuf,"%3d", (int)(elev+0.5) ); 
    else
      sprintf(sBuf,"%3d", -(int)(-elev+0.5) ); 
    display.print((char*)sBuf);

    display.printByte(CHAR_DEG);
    if( LastElev > -91.0 && nCntMinMax>0 )
    {
      if( elev>LastElev+0.0001 )      cUpDown = CHAR_UP;    // ^
      else if( elev<LastElev-0.0001 ) cUpDown = CHAR_DOWN;  // v
    }
    display.set_font(5,7,false);
    display.printByte(cUpDown);
    display.set_font(4,7,false);
    display.print(F(" "));

    // display sunrise?:
    if( bWithRiseSet )
    {
      if( Tnow>set || Tnow<rise )
      {
        dm( rise, &hh, &mm, &iv );
        hh = CorrTimeZone(hh);
        display.set_font(5,7,false);
        display.printByte(CHAR_UP);
        display.set_font(4,7,false);
        sprintf(sBuf, "%02d:%02d", hh, (int)mm );
        display.print((char*)sBuf);
      }
      else  // display sunset:
      {
        dm( set, &hh, &mm, &iv );
        hh = CorrTimeZone(hh);
        display.set_font(5,7,false);
        display.printByte(CHAR_DOWN);
        display.set_font(4,7,false);
        sprintf(sBuf, "%02d:%02d", hh, (int)mm );
        display.print((char*)sBuf);
      }
    }
    else if( Displays!=2 )
    {
      display.set_font(4,7,false);
      display.print( F(" --:--") );
    }
    display.print(F("    "));
    //display.rect(display.get_width()-5,0,display.get_width()-1,display.get_height()-1,0);
    display.set_font(5,7,false);
    LastElev = elev;
    nCntMinMax++;
  } 
}

void Output_SunRiseSet(int x, int y, int secs)
{
#ifdef ARDU_MEGA
  //byte cUpDown = ' ';
  bool bWithRiseSet = true;
  short nSwitch = secs/2;
  
  ShowTime = true;
  if( IsSynced )
  {
    sundata Sun=sundata(gpsLat, gpsLon, 0);
    Sun.time(year(),month(),day(),hour(),minute(),second());  // UTC
    Sun.calculations();
    float rise, set, Tnow;
    double mm;
    int hh, iv;
    rise = Sun.sunrise_time();
    set = Sun.sunset_time();
    if( isnan(rise) || isnan(set) ) bWithRiseSet = false;  // no rise/set times?
    
    Tnow = (float)hour()+1.0/60.0*minute();
    if( Tnow>set && bWithRiseSet )
    {
      // calc sun rise for next day:
      Sun.time(year(),month(),day()+1,hour(),minute(),second());  // UTC
      Sun.calculations();
      rise = Sun.sunrise_time();
    }

    display.setCursor(x,y);
    if( Displays==2 ) display.print(F(" "));
    display.set_font(7,7,false);
    display.printByte(CHAR_SUN);
    display.set_font(4,7,false);
    display.print(F(" "));

    // display sunrise?:
    if( bWithRiseSet )
    {
      if( (nCntMinMax/nSwitch)%2==0 )
  //  if( Tnow>set || Tnow<rise )
      {
        dm( rise, &hh, &mm, &iv );
        hh = CorrTimeZone(hh);
        display.set_font(5,7,false);
        display.printByte(CHAR_UP);
        display.set_font(4,7,false);
        sprintf(sBuf, "%02d:%02d", hh, (int)mm );
        display.print((char*)sBuf);
      }
      else  // display sunset:
      {
        dm( set, &hh, &mm, &iv );
        hh = CorrTimeZone(hh);
        display.set_font(5,7,false);
        display.printByte(CHAR_DOWN);
        display.set_font(4,7,false);
        sprintf(sBuf, "%02d:%02d", hh, (int)mm );
        display.print((char*)sBuf);
      }
    }
    else
    {
      display.set_font(4,7,false);
      display.print( F(" --:--") );
    }
    display.print(F("    "));
    display.set_font(5,7,false);
    nCntMinMax++;
  } 
#endif
}

//**** Outputs the phase of the moon as a number
void Output_MoonPercent(int x, int y)
{
  byte cSym = ' ';

  ShowTime = true;
  switch(moon_phase())
  { 
    case  0: cSym = CHAR_MOON100; break;  // Full
    case  1: cSym = CHAR_MOON83M; break;
    case  2: cSym = CHAR_MOON67M; break;
    case  3: cSym = CHAR_MOON50M; break;  // -50%
    case  4: cSym = CHAR_MOON33M; break;
    case  5: cSym = CHAR_MOON17M; break;
    case  6: cSym = CHAR_MOON0;   break;  // New
    case  7: cSym = CHAR_MOON17P; break;
    case  8: cSym = CHAR_MOON33P; break;
    case  9: cSym = CHAR_MOON50P; break;  // 50%
    case 10: cSym = CHAR_MOON67P; break;
    case 11: cSym = CHAR_MOON83P; break;
  };
  int nPhase = moon_percent();
  
  display.setCursor(x,y);
  if( Displays==2 ) display.print(F(" "));
  display.set_font(7,7,false);
  display.printByte(cSym);  // Moon with phase
  display.set_font(4,7,false);
  if( nPhase>=0 )
    {
      sprintf(sBuf, "%4d%%",  nPhase ); display.print((char*)sBuf); 
      display.set_font(5,7,false);
      if( nPhase>0 && nPhase<100 ) {
        display.printByte((byte)CHAR_UP); }   // up
      else { 
        display.print(F(" ")); }
    }
  else
    { 
      sprintf(sBuf, "%4d%%", -nPhase ); display.print((char*)sBuf); 
      display.set_font(5,7,false);
      if( nPhase<0 && nPhase>-100) {
        display.printByte((byte)CHAR_DOWN); } // down
      else {
        display.print(F(" ")); }
    }
  display.set_font(4,7,false);
  display.print(F("   "));
  if( Displays!=2 )
  {
    display.set_font(5,7,false);
    display.print(F("M"));
    display.set_font(4,7,false);
    display.print(F("ond      ")); 
  }
  display.set_font(5,7,false);
}


//**** 0=Temp:     " ##.#°C| Temp"
//     1=Humidity: " ##.#%|  Hum"
//     2=preasure: "####.#hPaPres"
void Output_Sensor(int x, int y, float Value, float Mean, float fOffset, short nWhat)
{
  static float Last_Value[3] = { -999.9, -999.9, -999.9 };
  byte  cUpDown;

  ShowTime = true;
  display.setCursor(x, y);
  if (isnan(Value))
  {
    //display.print(" DHT22 Fehler");
    Value = Last_Value[nWhat];
  }
  
  //**** Trend:
  cUpDown = ' ';
  if( Value>Mean+fSchwelle )      cUpDown = CHAR_UP;    // ^
  else if( Value<Mean-fSchwelle ) cUpDown = CHAR_DOWN;  // v
    
  if( nWhat<2 ) dtostrf(Mean+fOffset, 5, 1, sfloat);
  else          dtostrf(Mean+fOffset, 6, 1, sfloat);

  display.set_font(4,7,false);
  switch(nWhat)
  {
    case 0: if( Displays==2 ) display.space(5);
            display.space(1);
            sprintf(sBuf, " %s%cC   ", sfloat, CHAR_DEG );  sBuf[8] = 0;
            display.print((char*)sBuf);
            display.set_font(5,7,false);
            display.printByte(cUpDown);
            display.set_font(5,7,true);
            display.print(F("  "));
            if( Displays!=2 ) display.print(F("Temp"));
            break;
    case 1: if( Displays==2 ) display.space(10);
            sprintf(sBuf, "%s%%", sfloat ); display.print((char*)sBuf);
            display.set_font(5,7,false);
            display.printByte(cUpDown);
            display.set_font(4,7,true);
            display.print(F(" "));
            if( Displays!=2 ) display.print(F("Feuchte"));
            break;
#ifdef WEATHER_BME
    case 2: if( Displays==2 ) display.space(6);
            display.print((char*)sfloat);
            display.set_font(3,5,true);
            display.print(F("hP"));
            display._x -= 1;
            display.print(F("a"));
            display.set_font(5,7,false);
            display.printByte(cUpDown);
            display.set_font(4,7,true);
            if( Displays!=2 ) display.print(F(" Druck")); 
            break;
#endif
  }

  if (isnan(Value)) display.print(F("?  "));
  else
  {
    display.print(F("   "));
    Last_Value[nWhat] = Value;
  }
  display.set_font(5,7,false);
}


//**** Output of minimal and maximal temperature per day
//     for short display changing min/max for secs/2 seconds
void Output_TempMinMax(int x, int y, int secs)
{
  short nSwitch = secs/2;
  ShowTime = true;
  display.setCursor(x, y);

#ifdef LED_MATRIX
  // change display min, max?:
  if( Displays==2 )
  {
    display.space(8);
    if( (nCntMinMax/nSwitch)%2==0 )
      // min temperature:
      Output_ValueTempMinMax(temp_min, CHAR_DOWN, true );
    else
      // max temperature:
      Output_ValueTempMinMax(temp_max, CHAR_UP, true );
  }

  else  // both together
#endif
  {
    // min temperature:
    Output_ValueTempMinMax(temp_min, CHAR_DOWN, false );
     // max temperature:
    Output_ValueTempMinMax(temp_max, CHAR_UP, true );
  }
  display.print(F("   "));
  nCntMinMax++;
}


void Output_ValueTempMinMax(float value, char cUpDown, bool bWithUnit )
{
  dtostrf(value+fTempOffset, 5, 1, sfloat);
  if( sfloat[1]==' ' ) strcpy( sfloat, sfloat+2);
  else if( sfloat[0]==' ' ) strcpy( sfloat, sfloat+1);
  display.printByte(cUpDown);
  display.set_font(4,7,false);
  if( bWithUnit )
    sprintf(sBuf, "%s%cC ", sfloat, CHAR_DEG );
  else
    sprintf(sBuf, "%s%c  ", sfloat, CHAR_DEG );
  display.print((char*)sBuf);
  display.set_font(5,7,false);
}


//**** Output of minimal and maximal humidity per day
//     for short display changing min/max for secs/2 seconds
void Output_HumMinMax(int x, int y, int secs)
{
  short nSwitch = secs/2;
  ShowTime = true;
  display.setCursor(x, y);

#ifdef LED_MATRIX
  // change display min, max?:
  if( Displays==2 )
  {
    display.space(8);
    Serial.print(hh_mean);
    Serial.print(" ");
    Serial.print(hum_min);
    Serial.print(" ");
    Serial.println(hum_max);
    if( (nCntMinMax/nSwitch)%2==0 )
      // min humidity:
      Output_ValueHumMinMax(hum_min, CHAR_DOWN, true );
    else
      // max humidity:
      Output_ValueHumMinMax(hum_max, CHAR_UP, true );
  }

  else  // both together
#endif
  {
    // min humidity:
    Output_ValueHumMinMax(hum_min, CHAR_DOWN, false );
     // max humidity:
    Output_ValueHumMinMax(hum_max, CHAR_UP, true );
  }
  display.print(F("   "));
  nCntMinMax++;
}


void Output_ValueHumMinMax(float value, char cUpDown, bool bWithUnit )
{
  dtostrf(value+fHumOffset, 5, 1, sfloat);
  if( sfloat[1]==' ' ) strcpy( sfloat, sfloat+2);
  else if( sfloat[0]==' ' ) strcpy( sfloat, sfloat+1);
  display.printByte(cUpDown);
  display.set_font(4,7,false);
  if( bWithUnit )
    sprintf(sBuf, "%s%%  ", sfloat );
  else
    sprintf(sBuf, "%s%%  ", sfloat );
  display.print((char*)sBuf);
  display.set_font(5,7,false);
}


bool Output_Text( char *line1, char *line2, int OnlyEach )
{
#ifdef ARDU_MEGA  // not LCD

  static short nCount=100;

  if( bGPSrunning || !bDispOn ) return true;  // not if GPS is running

  nCount++;
  if( nCount<OnlyEach ) return true;

  nCount = 0;
  if( ShowTime )
  {
     ShowTime = false;
     display.clear(); //Clears the LCD screen and positions the cursor in the upper-left  corner.
  }
  //**** Laufschrift:
  display.setCursor(Display_x-1,0); // set the cursor to column 15, line 0
  #ifndef LED_MATRIX
  int t;
  int Wait = 4; // wait 4 sec after displaying text (LCD)
  for (int positionCounter1 = 0; positionCounter1 < strlen(line1); positionCounter1++)
  {
    display.scrollDisplayLeft(); //Scrolls the contents of the display one space to the left.
    display.printByte(ReplaceChar(line1[positionCounter1])); // Print a message to the LCD.
    delay(tim); //wait for microseconds
    Output_Blink(second());
    if( CheckButtons() ) { display.clear(); ShowTime = true; return false; }
  }

  //**** GPS:
  GetGPS(0);

  display.clear(); //Clears the LCD screen and positions the cursor in the upper-left  corner.
  display.setCursor(Display_x-1,1); // set the cursor to column 15, line 1
  for (int positionCounter = 0; positionCounter < strlen(line2); positionCounter++)
  {
    display.scrollDisplayLeft(); //Scrolls the contents of the display one space to the left.
    display.printByte(ReplaceChar(line2[positionCounter])); // Print a message to the LCD.
    delay(tim); //wait for microseconds
    Output_Blink(second());
    if( CheckButtons() ) { display.clear(); ShowTime = true; return false; }
  }
  //secLastSync += actSec()-nss+Wait;

  for( t=0; t<Wait*1000; t+=500 )
  {
    //**** GPS:
    GetGPS(0);
    delay(500); //wait for microseconds
    if( CheckButtons() ) { display.clear(); ShowTime = true; return false; }
  }
  #else
  char buf[70]; strcpy( buf, line1 ); strcat( buf, line2 );
  display.set_font(5,7,true);
  display.scrolltext(4, buf, 5, 1, 0);  // y, Text, delay, repeat, dir
  display.setCursor(0,0);
  #endif

  display.clear();
  display.set_font(5,7,false);
#endif
  ShowTime = true;
  return true;
}


uint8_t ReplaceChar(unsigned char In )
{
  // does not work, is UTF-8
  static uint8_t cRet=' ';

  cRet = (uint8_t)In;
  if( In==132 ) cRet = 0xE1;    // 'ä'
  if( In==148 ) cRet = 0xEF;    // 'ö'
  if( In==154 ) cRet = 0xF5;    // 'ü'
  if( In==225 ) cRet = 0xE2;    // 'ß'
  if( In==248 ) cRet = CHAR_DEG;// '°'
//if( In=='µ' ) cRet = 0xE4;  // ?
//if( In=='Ω' ) cRet = 0xF4;  // ?
  
  return cRet;
}


//**** Modulo Funktion:
int imod(int z1, int z2) {
  int ze;
  ze=z1 % z2;
  if (ze>=0) return(ze);
  else       return(z2+ze);
}


//********* convert decimal h -> h,m,sign:
void dm( double deg, int *ih, double *dm, int *iv)
{
  *iv  = ( deg>=0.0 ? 1 : -1 );
  if( *iv>0 ) *dm =  deg+1.0e-8;
  else        *dm = -deg+1.0e-8;
  *ih  = (int)*dm;
  *dm = (*dm-*ih)*60.0;
}


//********* convert decimal h -> h,m,sec,sign:
void dms( double deg, int *ih, int *im, double *sec, int *iv)
{
  *iv  = ( deg>=0.0 ? 1 : -1 );
  if( *iv>0 ) *sec =  deg+1.0e-8;
  else        *sec = -deg+1.0e-8;
  *ih  = (int)*sec;
  *sec = (*sec-*ih)*60.0;
  *im  = (int)*sec;
  *sec = (*sec-*im)*60.0;
}


//********* Output "+dd mm ss " to string:
char *cDms( double dVal )
{
    static char sRet[30];
    //const char  *vorz="-++";
    int h, m, v;
    double s;

    dms( dVal, &h, &m, &s, &v );
    sprintf( sRet, "%02d:%2.2d:%02d ", h, m, (int)(s+0.5) );

    return sRet;
}

/************************************************************/
