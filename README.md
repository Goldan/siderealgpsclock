# SiderealGPSClock

Sidereal clock, with GPS base for Arduino. Display on LCD modul or LED-Matrix
 
Last update: 2021-12-04

Possible outputs:
- geogr. coordinates
- time
- date
- week day
- julian date
- mean local time
- true local time
- sidereal time
- moon phase
- temperature
- humidity
- running textes

This outputs could be configured for yes/no, order or duration.

The file structure to compile must be:
|Files|
| --- |
|siderealgpsclock\siderealgpsclock.ino|
|siderealgpsclock\src\ht1632c\ht1632c.cpp|
|siderealgpsclock\src\ht1632c\ht1632c.h|
|siderealgpsclock\src\ht1632c\ht1632c_font.h|
|siderealgpsclock\src\ht1632c\ht1632c_font_3x5.h|
|siderealgpsclock\src\ht1632c\ht1632c_font_4x7.h|
|siderealgpsclock\src\ht1632c\ht1632c_font_5x7.h|
|siderealgpsclock\src\ht1632c\ht1632c_font_5x12.h|
|siderealgpsclock\src\ht1632c\ht1632c_font_7x7.h|

More pictures and infos under https://www.goldan.org/juergen/software/#Siderial
and a video of this clock https://www.youtube.com/watch?v=n8zJUUFGRc0&t=2s


# Wiring for Arduino Mega and two LED Matrix displays

![Alt text](/Docu/GPSClock_Mega_2LED_Steckplatine.png)

Needed devices for the version MEGA with LED:
- Arduino MEGA
- Display: LED 16x24 Pixel (up to 3 panals) (https://www.amazon.de/gp/product/B07Z7T9L5G/ref=ppx_yo_dt_b_asin_image_o04_s00?ie=UTF8&psc=1)
- GPS: p.e. AZDelivery NEO-6M GPS Modul (https://www.amazon.de/gp/product/B01N38EMBF)
- Weather: BME280 (https://www.amazon.de/gp/product/B07D8T4HP6/ref=ppx_yo_dt_b_asin_title_o01_s00?ie=UTF8&th=1)

Needed libraries:
- Adafruit_BME280_library
- digitalWriteFast
- HT1632C - included in project, see folder /src
- Sundata-master
- Time-1.5.0
- TinyGPSPlus-master (http://arduiniana.org/libraries/tinygpsplus/)
- Adafruit_Unified_Sensor (https://www.arduino.cc/reference/en/libraries/adafruit-unified-sensor/)
 
Connectors:

| LED-Display  | Arduino MEGA |
| --- | --- |
| Pin 1 VCC  | 3.3V                    |
| Pin 2 GND  | GND                     |
| Pin 3 DAT  | ANALOG_IN A0            |
| Pin 4 #WR  | ANALOG_IN A1            |
| Pin 5 #RD  | ANALOG_IN A2 (not used) |
| Pin 6 #CS1 | ANALOG_IN A3 for panel 1|
| Pin 6 #CS2 | ANALOG_IN A4 for panel 2|
| Pin 6 #CS3 | ANALOG_IN A5 for panel 3 (optional)|

| BME280  |   Arduino MEGA |
| --- | --- |
| Pin 1 5V  | 5V  |
| Pin 2 Gnd | GND |
| Pin 3 SCL | 21  |
| Pin 4 SDA | 20  |


# Wiring with Arduino UNO and one LCD Display

![Alt text](/Docu/GPSClock_UNO_LCD_Steckplatine.png)

Needed devices for the version with UNO with LCD:
- Arduino UNO
- Display: I2C LCD1602, 16x2 characters (https://www.sunfounder.com/i2clcd.html)
- GPS: p.e. AZDelivery NEO-6M GPS Modul (https://www.amazon.de/gp/product/B01N38EMBF)
- Weather: DHT11/22 (https://www.amazon.de/DHT22-AM2302-Digital-Temperatur-Feuchtesensor/dp/B01DB8JH4M)

Needed libraries:
- LiquidCrystal_I2C
- DHT_sensor_library
- digitalWriteFast
- Sundata-master
- Time-1.5.0
- TinyGPSPlus-master (http://arduiniana.org/libraries/tinygpsplus/)
- SoftwareSerial (only UNO)
 
Connectors:

| LCD-Display  | Arduino UNO |
| --- | --- |
| Pin 1 GND | GND         |
| Pin 2 VCC | 5V          |
| Pin 3 SDA | ANALOG_IN A4|
| Pin 4 SDL | ANALOG_IN A5|

| DHT11/22     | Arduino UNO |
| --- | --- |
| Pin 1 3.3-5V | 3.3V |
| Pin 2 Data   | ANALOG_IN A4, with R4,7K to +3.3V |
| Pin 3 Gnd    | GND |
| Pin 4 Gnd    | GND |


## For both configuration are needed:

| GPS        | Arduino    |
| --- | --- |
| Pin 1 3.3V | 3.3V       |
| Pin 2 RX   | DIGITAL D4 |
| Pin 3 TX   | DIGITAL D5 |
| Pin 4 Gnd  | GND        |


Blinking LED for seconds flashing if needed:
| LED       | Arduino    |
| --- | --- |
| red       | DIGITAL D6 |


Buttons, all switches to LOW (Gnd):
| Buttons |MEGA / 3 LED|MEGA / 2 LED|UNO / LCD|
| --- | --- | --- | --- |
| off (long press), brightness (short press) |D51|D51| D8 |
| UTC                 |D49| - | D9 (optional)|
| MOZ                 |D47| - |D10 (optional)|
| WOZ                 |D45| - |D11 (optional)|
| STZ                 |D43| - |D12 (optional)|
| change times, others|D41|D41|D13|
| change sun/moon     |D39| - | - |
| change weather      |D37| - | - |


The source code is the same for all configuration. For configurations are #define at the start up in the code.
|var| #define | #undef |
| --- | --- | --- |
|**ARDU_MEGA**| code for MEGA | code for UNO |
| Depending of this settings| the display and weather sensor | are configured as above| 
|**VS**|with 3 LED matrix for Volkssternwarte Hannover|all others |
|**BLINK**|for blinking LED if connected | not used |
|**DEBUG**|for debug outputs on Serial | |
|Set the used sign for separator in time output:|||
|**SEPARATOR**|':' normal double sign as separator|##:##:##|
|| '.' only one point in the middle|##.##.##|
|| ' ' space as separator|## ## ## |
|**BLINK_DP**|for blinking separators in times like ##:##:##|static double points|
|**AUTO_RESET**|reset arduino at 04:00:00 each day|no reset|

In the night the display is switched off. If the arduino code is started, you will see the running text
and than the display will switching off. To turn the display on, one of the buttons has to be pushed.

The actual output for the display was stored in **nDisplayMode**.
The possible values are
| Value | Description |
| --- | --- |
|  0 | GPS position lat, lon |
|  1 | date like dd.mm.yy |
|  2 | week date like Montag, .. |
|  3 | analog clock |
|  4 | UTC |
|  5 | number of julian day |
|  6 | MOZ |
|  7 | WOZ |
|  8 | ZGL (difference MOZ-WOZ |
|  9 | STZ, sidereal time |
| 10 | hight of sun |
| 11 | time of rise and set of the sun |
| 12 | phase of the moon as text |
| 13 | phase of the moon as number |
| 14 | temperature |
| 15 | min and max temperature of the day (reset at 04:00:0) |
| 16 | humidity |
| 17 | air preasure |
| 18 | running text |
  
Important variables:
| Variable | Description |
| --- | --- |
| short nMapper      | how long each output is shown in seconds, 0=output is off, for running text how often the text is shown|
| short nMenuSunMoon | array to set the order of outputs in this order for button **sun/moon** |
| short nMenuWeather | array to set the order of outputs weather for button **weather** |
| short nMenuOthers  | array to set the order of outputs in this order for button **times, etc** |


Contact: 
juergen.goldan(at)gmail.com

